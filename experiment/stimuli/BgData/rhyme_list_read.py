#! /bin/env python3

def normalize_pair(a, b):
	if a > b:
		return (b, a)
	return (a, b)

rhyme_pairs = {None : set()}
words = set()

with open('rhyming_words_compilation.txt', 'r') as rhymes:
	lines = rhymes.readlines()
	lines.reverse()
	#print (lines)
	for l in lines:
		l = l.split()
		#print (l)
		if len(l) >= 1:
			a, b = normalize_pair(l[0], l[1])
			if a not in words and b not in words:
				rhyme_pairs[None].add((a,b))
				words.add(a)
				words.add(b)
			#print (a, b,)
		else:
			continue
			
	print (rhyme_pairs[None], len(rhyme_pairs[None]))
