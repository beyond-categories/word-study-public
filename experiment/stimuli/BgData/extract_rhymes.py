#! /bin/env/ python
#   This is a script
'''
'''

import csv
import re
import random

########################################################################################
#   Private global data
SAMPA_vowels = {
                        'A',
                        'i',
                        'I',
                        'E',
                        '3`'
                        '{',
                        'Ar',
                        'V',
                        'O',
                        'U',
                        'u',
                        #'@',
                        #'@`',
                        
                        'u:',
                        'U@',
                        'E@',
                        'I@',
                        'aU',
                        '@U',
                        'OI',
                        'aI',
                        'eI',
                }

#   Compile a regex pattern that will be used later, so that it is faster when used
vowelplus = re.compile('('+'|'.join(list(SAMPA_vowels))+')+.*')

########################################################################################
#   Helper methods declared and defined in advance.

def extract_syllables(string):
    return string.split('.')

def extract_morphemes(string):
    return filter(None, re.split('[^a-zA-Z]*(?=[a-zA-Z]*)', string))
    
#   Applies regex pattern to extract the rhyme: {nucleus and coda} of a given syllable
def extract_rhyme(syllable):
    try:
        #print syllable, re.search(vowel, syllable).group(), "\n"
        return re.search(vowelplus, syllable).group()
    except AttributeError:
        return None
    
########################################################################################

try:

    rows = []

    with open('I152726.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            rows.append(row)

    outfile = open('rhyming_words.txt' , 'w')
    outfile.close()
  
    print (rows[0], "\n")
    print (rows[1])
    print (rows[1][0], ":", extract_morphemes(rows[1][18]), extract_syllables(rows[1][15]), "\n")
        # https://stackoverflow.com/questions/1059559/split-strings-with-multiple-delimiters

    rhyming_word_dict = {}

    #   ####
    #   Iterate over all words in the word list to find out any rhyming words

    random.shuffle(rows)

    counter = 200
    for index_1 in range(1, len(rows)):
        if counter == 0:
            break
        
        
        if len(rows[index_1]) < 18:
            continue
  
        word_1 = rows[index_1][0]

        if len(word_1) < 4 or len(word_1) > 10 or word_1 != word_1.lower() or "NN" not in rows[index_1][21] or float(rows[index_1][5]) < 5:
            continue

        morphemes_1 = extract_morphemes(rows[index_1][18])
        syllables_1 = extract_syllables(rows[index_1][15])

        # Skip this word if it ends in a vowel (for rhyming words selection this can be ambiguous)
        ends_in_vowel = False
        for vowel in SAMPA_vowels:
            if syllables_1[len(syllables_1)-1].endswith(vowel):
                ends_in_vowel = True
                #print (word_1,syllables_1) #!!!!!!!!!!!
                break
        if ends_in_vowel:
            continue

        executed = False
        # Nested for-loop to enable 1-1 comparisons
        for index_2 in range(1, len(rows)):
            if len(rows[index_2]) < 18:
                continue
      
            word_2 = rows[index_2][0]

            if word_1.lower() == word_2.lower():
                continue
  
            if len(word_2) < 4 or len(word_2) > 10 or word_2 != word_2.lower() or "NN" not in rows[index_2][21] or float(rows[index_2][5]) < 5 or (len(word_2) - len(word_1) < 0):
                continue
  
            morphemes_2 = extract_morphemes(rows[index_2][18])
            syllables_2 = extract_syllables(rows[index_2][15])
  
            # We don't want this to be vowel-terminating either
            ends_in_vowel = False
            for vowel in SAMPA_vowels:
                if syllables_2[len(syllables_2)-1].endswith(vowel):
                    ends_in_vowel = True
                    break
            if ends_in_vowel:
                continue
            
            # To keep track of the progress
            #print (index_1,index_2)
            #### Tests go here, and further action further below.
            
            #   To check if the `rhyme' parts of the last syllables of the words match.
            rhyme_1 = extract_rhyme(syllables_1[len(syllables_1)-1])
            rhyme_2 = extract_rhyme(syllables_2[len(syllables_2)-1])

            
            if  rhyme_1 == rhyme_2 and rhyme_1 != None and "IN" not in rhyme_1 and rhyme_1[-1] not in '`sz':
                print ("rhyming!\t", word_1, word_2, "\t%s"%extract_rhyme(syllables_1[len(syllables_1)-1]), " %s"%extract_rhyme(syllables_2[len(syllables_2)-1]) )
                #rhyming_word_dict[word_1] = rhyming_word_dict.get(word_1,set()).union({word_2})

                outfile = open('rhyming_words.txt' , 'a')
                outfile.write(word_1 + "\t" + word_2 + "\n")
                outfile.close()

                executed = True

        if executed:
            counter -= 1
            outfile = open('rhyming_words.txt' , 'a')
            outfile.write("\n")
            outfile.close()
        print(counter)

        #print (rhyming_word_dict)


        
                
except KeyboardInterrupt:
    print ("\n\nExiting due to KeyboardInterrupt.\n")
    raise SystemExit

########################################################################################
