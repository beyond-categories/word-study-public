#! /bin/env python

# This file may be used to process a list of words
# and output them to another file in the format:
# Word1     
# Word2     
# ...           
#
# This format is to be used with a supplementary python module
# for eventual use with a presentation experiment using
# the corresponding hash values is a convenient way to
# assign fixed port codes without having to hard-code them.
# Unfortunately presentation has no functionality that can
# do anything near this so such an external file needs to be
# generated and read into presentation. Optionally, this task
# may be done at runtime on systems already having python,
# or may be pre-generated to be on the safe side.

import re

def func():
	
	f = open('count_1w.txt','r')
	txt = f.read()
	txt = re.sub( r'([a-zA-Z])([0-9])', r'\1\t\t\t\2', txt )
	f.close()
	f = open('count_1w.out.txt','w')
	f.write(txt)
	f.close()

func()
