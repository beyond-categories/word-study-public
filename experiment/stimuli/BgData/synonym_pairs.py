#! /bin/env/ python
#   This is a script
'''
'''

import csv
import re

def normalize_pair(a, b):
	if a > b:
		return (b, a)
	return (a, b)

########################################################################################
#   Private global data
synonym_pairs = {
"ROGUE" :	"SCOUNDREL",
"AVERAGE" :	"TYPICAL",
#"COFFEE" :	"TEA",
"RIVER" :	"STREAM",
#"HUMOUR" :	"WIT",
#"ENAMEL" :	"COATING",
#"PLANT" :	"TREE",
"DISTANCE" :    "LENGTH",
"PAIR" :	"COUPLE",
"BASIC" :	"SIMPLE",
#"JEWEL" :	"GEM",
"FASHION" :	"STYLE",
"AUDIT" :	"INSPECTION",
"CONSIDER" :	"THINK",
"STUDENT" :	"PUPIL",
#"IMPETUS" :	"MOTIVATION",
#"CHILD" :	"KID",
"LOBSTER" :	"CRAYFISH",
"BOREDOM" :	"DULLNESS",
#"CAUSE" :	"MAKE",
#"ZIPPER" :	"FASTENER",
#"CLEAN" :	"WASH",
"RELIGION" :    "FAITH",
#"PROTOCOL" :    "ETIQUETTE",
"PROPERTY" :    "BUILDING",
"STRENGTH" :    "POWER",
#"CONSTANT" :    "REGULAR",
"INTERIM" :	"TEMPORARY",
#"TELEPHONE" :   "RADIO",
#"VERITY" :	"CERTAINTY",
"MOTHER" :	"PARENT",
"VALUE" :	"PRICE",
"QUAKE" :	"TREMOR",
#"MONEY" :	"CASH",
"PROPER" :	"APPROPRIATE",
"SHRIMP" :	"PRAWN",
#"MASTER" :	"PROFESSOR",
#"DEITY" :	"DIVINITY",
"PATTERN" :	"DESIGN",
"FOREST" :	"WOODS",
#"ORDINARY" :	"NORMAL",
#"PROBLEM" :	"DIFFICULTY",
"FROG" :	"TOAD",
#"GALLANT" :	"HEROIC",
"FREEDOM" :	"INDEPENDENCE",
"ROAD" :	"STREET",
#"VIOLIN" :	"VIOLA",
"EFFECT" :	"CONSEQUENCE",
"ALIAS" :	"PSEUDONYM",
"PRIVATE" :	"PERSONAL",
#"ARBITER" :	"MEDIATOR",
"REASON" :	"EXPLANATION",
#"TULIP" :	"DAFFODIL",
"ROCK" :	"STONE",
"ADVANTAGE" :	"BENEFIT",
#"ATTRIBUTE" :	"TRAIT",
"BROAD" :	"WIDE",
"FUNCTION" :	"PURPOSE",
#"EXPANSE" :	"VASTNESS",
#"HOSTILITY" :	"AGGRESSION",
"BUTTERFLY" :	"MOTH",
#"WINDOW" :	"DOOR",
#"HELMET" :	"HEADDRESS",
"OPPONENT" :    "FOE",
"FALLACY" :	"MYTH",
"EDUCATION" :   "TEACHING",
"SOCIETY" :	"PUBLIC",
"TENDENCY" :    "TREND",

"lawyer" : "attorney",
"autmun" : "fall",
"penny" : "cent",
"taxi" : "cab",
"sorrow" : "depression",
"beauty" : "grace",
"danger" : "hazard",
"anxiety" : "distress",
"mind" : "brain",

'infant': 'baby', 'sentence': 'phrase', 'money': 'currency', 'mission': 'task', 'trade': 'commerce', 'country': 'nation', 'human': 'person', 'research': 'investigation', 'debate': 'discussion', 'passage': 'corridor', 'program': 'schedule', 'consent': 'permission', 'complaint': 'grievance', 'couple': 'pair', 'courage': 'bravery', 'basement': 'cellar', 'trouble': 'difficulty', 'warfare': 'combat', 'desire': 'wish', 'fortune': 'wealth', 'panic': 'fear', 'candy': 'sweets', 'lightning': 'thunder', 'legend': 'myth', 'crossing': 'intersection', 'river': 'stream', 'vision': 'sight', 'sequence': 'series', 'liquor': 'alcohol', 'idea': 'concept', 'seat': 'chair', 'design': 'pattern', 'result': 'outcome', 'empire': 'kingdom', 'witness': 'observer', 'subject': 'topic', 'savage': 'barbarian', 'profit': 'gain', 'movie': 'film', 'poison': 'venom', 'forecast': 'prediction', 'tribute': 'praise', 'method': 'procedure', 'power': 'strength', 'poem': 'verse', 'pressure': 'force', 'teacher': 'instructor', 'license': 'permit', 'column': 'pillar', 'freedom': 'liberty', 'region': 'area', 'drama': 'play', 'soldier': 'warrior', 'logic': 'reasoning', 'garbage': 'trash', 'presence': 'existence', 'echo': 'reverberation', 'vessel': 'ship', 'crisis': 'emergency', 'razor': 'shaver', 'story': 'tale', 'custom': 'tradition', 'moisture': 'dampness', 'meeting': 'assembly', 'minor': 'juvenile', 'engine': 'motor', 'murder': 'homicide', 'stomach': 'belly', 'prison': 'jail', 'worker': 'laborer', 'missile': 'projectile', 'revolver': 'pistol', 'harvest': 'crop', 'present': 'gift', 'pilot': 'aviator', 'novel': 'book', 'target': 'goal', 'tunnel': 'cave', 'value': 'worth', 'lumber': 'wood', 'marriage': 'wedding', 'error': 'mistake', 'college': 'university', 'feeling': 'emotion', 'control': 'regulation', 'fashion': 'style', 'palace': 'castle', 'soil': 'dirt', 'defect': 'flaw', 'honey': 'nectar', 'player': 'contestant', 'anger': 'wrath', 'cafe': 'restaurant', 'trip': 'journey', 'capture': 'seizure', 'author': 'writer', 'patent': 'copyright', 'index': 'listing', 'damage': 'harm', 'actor': 'performer', 'member': 'fellow', 'answer': 'reply', 'smell': 'odor', 'tension': 'strain', 'plant': 'tree', 'normal': 'usual', 'effect': 'influence', 'bundle': 'package', 'delight': 'pleasure', 'fatigue': 'tiredness', 'purpose': 'intention', 'ethics': 'morals', 'singer': 'vocalist', 'liquid': 'fluid', 'disease': 'illness', 'jacket': 'coat', 'pupil': 'student', 'quantity': 'number',
}

normalized = set()
for key in synonym_pairs:
    v = synonym_pairs[key]
    normalized.add(normalize_pair(key.lower(), v.lower()))

print ((normalized))


