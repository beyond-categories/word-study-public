#! /bin/env/ python3

# Standard GNU GPL v3 license statement
# You may copy, distribute and modify the software
# as long as you track changes/dates in source files.
# Any modifications to or software including (via compiler)
# GPL-licensed code must also be made available under
# the GPL along with build & install instructions.

import csv
import heapq
import pickle

rDict = dict()
with open('Ratings_Warriner_et_al.csv', newline='') as csvfile:
    ratings = csv.DictReader(csvfile, delimiter=',')
    for row in ratings:
        rDict.update({row['Word'] : row['V.Mean.Sum']})

print (len(rDict))

total_sentiment = 0.0
positive_sentiment = 0.0
negative_sentiment = 0.0
threshold = 1.5
positive_words = []
negative_words = []

inDirectory = "../../CounterbalancedBlocks/"
for i in {1,2,3,4,5,6}:
    with open(inDirectory + "%d/%dlist.txt" %(i,i)) as word_list:
        for line in word_list:
            sentiment = float(rDict.get(line.split()[2], 4.5)) - 1 - 3.5
            if abs(sentiment) < threshold:
                continue 
            total_sentiment += sentiment
            if sentiment > 0:
                heapq.heappush(positive_words, (3.5-abs(sentiment), line.split()[2], sentiment))
                positive_sentiment += sentiment
            else:
                heapq.heappush(negative_words, (3.5-abs(sentiment), line.split()[2], sentiment))
                negative_sentiment += sentiment
                
pickle.dump(positive_words, open("./positive_words.heapq.pickle", "wb"))
pickle.dump(negative_words, open("./negative_words.heapq.pickle", "wb"))
                
print("Total sentiment:\t%s\nPositive sentiment:\t%s\nNegative sentiment:\t%s" % (total_sentiment, positive_sentiment, negative_sentiment))
print("Number of positive words above threshold %f:\t%d" % (threshold, len(positive_words)))
print("Number of negative words above threshold %f:\t%d" % (threshold, len(negative_words)))

for i in range(min(100,min(len(positive_words),len(negative_words)))):
    print('{!s:<15} {:<20}\t{!s:<15} {:<20}'.format(*(heapq.heappop(positive_words)[1:] + heapq.heappop(negative_words)[1:])))
