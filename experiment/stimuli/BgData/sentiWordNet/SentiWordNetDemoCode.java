//    
//    Modified 2018, Aalok Sathe
//    for use at the University of Richmond's dept. of psychology,
//    Beyond Categories lab.
//
//    Copyright 2013 Petter Tornberg
//
//    This demo code has been kindly provided by Petter Tornberg <pettert@chalmers.se>
//    for the SentiWordNet website.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import java.util.PriorityQueue;
import java.util.Comparator;

import java.lang.*;

public class SentiWordNetDemoCode {

	private Map<String, Double> dictionary;

	public SentiWordNetDemoCode(String pathToSWN) throws IOException {
		// This is our main dictionary representation
		dictionary = new HashMap<String, Double>();

		// From String to list of doubles.
		HashMap<String, HashMap<Integer, Double>> tempDictionary = new HashMap<String, HashMap<Integer, Double>>();

		BufferedReader csv = null;
		try {
			csv = new BufferedReader(new FileReader(pathToSWN));
			int lineNumber = 0;

			String line;
			while ((line = csv.readLine()) != null) {
				lineNumber++;

				// If it's a comment, skip this line.
				if (!line.trim().startsWith("#")) {
					// We use tab separation
					String[] data = line.split("\t");
					String wordTypeMarker = data[0];

					// Example line:
					// POS ID PosS NegS SynsetTerm#sensenumber Desc
					// a 00009618 0.5 0.25 spartan#4 austere#3 ascetical#2
					// ascetic#2 practicing great self-denial;...etc

					// Is it a valid line? Otherwise, throw exception.
					if (data.length != 6) {
						throw new IllegalArgumentException(
								"Incorrect tabulation format in file, line: "
										+ lineNumber);
					}

					// Calculate synset score as score = PosS - NegS
					Double synsetScore = Double.parseDouble(data[2])
							- Double.parseDouble(data[3]);

					// Get all Synset terms
					String[] synTermsSplit = data[4].split(" ");

					// Go through all terms of current synset.
					for (String synTermSplit : synTermsSplit) {
						// Get synterm and synterm rank
						String[] synTermAndRank = synTermSplit.split("#");
						String synTerm = synTermAndRank[0] + "#"
								+ wordTypeMarker;

						int synTermRank = Integer.parseInt(synTermAndRank[1]);
						// What we get here is a map of the type:
						// term -> {score of synset#1, score of synset#2...}

						// Add map to term if it doesn't have one
						if (!tempDictionary.containsKey(synTerm)) {
							tempDictionary.put(synTerm,
									new HashMap<Integer, Double>());
						}

						// Add synset link to synterm
						tempDictionary.get(synTerm).put(synTermRank,
								synsetScore);
					}
				}
			}

			// Go through all the terms.
			for (Map.Entry<String, HashMap<Integer, Double>> entry : tempDictionary
					.entrySet()) {
				String word = entry.getKey();
				Map<Integer, Double> synSetScoreMap = entry.getValue();

				// Calculate weighted average. Weigh the synsets according to
				// their rank.
				// Score= 1/2*first + 1/3*second + 1/4*third ..... etc.
				// Sum = 1/1 + 1/2 + 1/3 ...
				double score = 0.0;
				double sum = 0.0;
				for (Map.Entry<Integer, Double> setScore : synSetScoreMap
						.entrySet()) {
					score += setScore.getValue() / (double) setScore.getKey();
					sum += 1.0 / (double) setScore.getKey();
				}
				score /= sum;

				dictionary.put(word, score);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (csv != null) {
				csv.close();
			}
		}
	}

	public double extract(String word, String pos) {
		return dictionary.get(word + "#" + pos);
	}
	
	private static class Word {
	    private String word;
	    private Double sentiment;
	    
	    public Word(String string, double doub) {
	        this.word = string;
	        this.sentiment = new Double(doub);
	    }
	    
	    public String getWord() {
	        return this.word;
	    }
	    
	    public double getSentiment() {
	        return this.sentiment.doubleValue();
	    }
	}
	
	public static void main(String [] args) throws IOException {
		/*if(args.length<1) {
			System.err.println("Usage: java SentiWordNetDemoCode <pathToSentiWordNetFile>");
			return;
		}*/
		
		
		String pathToSWN = "swndump.txt";// args[0];
		SentiWordNetDemoCode sentiwordnet = new SentiWordNetDemoCode(pathToSWN);
		
		/*
		System.out.println("good#a "+sentiwordnet.extract("good", "a"));
		System.out.println("bad#a "+sentiwordnet.extract("bad", "a"));
		System.out.println("blue#a "+sentiwordnet.extract("blue", "a"));
		System.out.println("blue#n "+sentiwordnet.extract("blue", "n"));
		*/
		
		double threshold = 0.1;
		
		double totalSentiment = 0.0;
		double totalPositiveSentiment = 0.0;
		double totalNegativeSentiment = 0.0;
		int positiveWords = 0;
		int negativeWords = 0;
		double numLines = 0;
				
		Comparator<Word> sentimentComparator = new Comparator<Word>() {
	        public int compare(Word w1, Word w2) {
	            return (-1)*(new Double(Math.abs(w1.getSentiment()))).compareTo((new Double(Math.abs(w2.getSentiment()))));
	        }
	    };
		PriorityQueue<Word> positivesQ = new PriorityQueue<>(11, sentimentComparator);
		PriorityQueue<Word> negativesQ = new PriorityQueue<>(11, sentimentComparator);
		
		for (int blockNum = 1; blockNum <= 6; blockNum++) {
		    BufferedReader csv = null;
		    //try {
			    csv = new BufferedReader(new FileReader(String.format("../../CounterbalancedBlocks/%d/%dlist.txt", blockNum, blockNum)));
			    
			    String line;
			    while ((line = csv.readLine()) != null) {
				    //numLines += 1.;
				    
				    //System.out.println(line);
				    String word = line.split("\t")[2];
				    
				    double sentiment;
				    try {
				        sentiment = sentiwordnet.extract(word, "n");
				        
				        if (Math.abs(sentiment) < Math.abs(threshold))
				            continue;
				        
				        //System.out.println(""+word+"#n"+sentiment+"\n");        			    
				        totalSentiment += sentiment;
				        
				        if (sentiment >= 0 && Math.abs(sentiment) >= Math.abs(threshold)) {
				            positiveWords += 1;
				            totalPositiveSentiment += sentiment;
				            positivesQ.add(new Word(word, sentiment));
				        }
				        else if (sentiment < 0 && Math.abs(sentiment) >= Math.abs(threshold)) {
				            negativeWords += 1;
				            totalNegativeSentiment += sentiment;
				            negativesQ.add(new Word(word, sentiment));
				        }
				        numLines += 1.;
				    } catch (Exception e) {
				        //e.printStackTrace();
				        if (false)
				            try {
				                sentiment = sentiwordnet.extract(word, "a");
				                
				                if (Math.abs(sentiment) <= Math.abs(threshold))
				                    continue;
				                
				                System.out.println(""+word+"#a"+sentiment+"\n");	
				                			    
				                totalSentiment += sentiment;
				                
				                if (sentiment >= 0) {
				                    positiveWords += 1;
				                    totalPositiveSentiment += sentiment;
				                }
				                else {
				                    negativeWords += 1;
				                    totalNegativeSentiment += sentiment;
				                }    
				                numLines += 1.;
				                
			                } catch (Exception eprime) {
			                    //eprime.printStackTrace();
			                } // endcatch
				    } // endcatch
				    
                } // endwhile
                
		} // endfor 
		
		System.out.println("\n\napproximate net sentiment:\t" + totalSentiment);            
    	System.out.println("average sentiment per word:\t" + totalSentiment/numLines);
    	System.out.println("positive sentiment:\t\t" + totalPositiveSentiment);
    	System.out.println("negative sentiment:\t\t" + totalNegativeSentiment);
    	System.out.println("positive words:\t\t\t" + positiveWords);
    	System.out.println("negative words:\t\t\t" + negativeWords);
    	
    	System.out.printf("\nThere are %s positive words above threshold %f\n", positivesQ.size(), threshold);
    	for (int i=0; i<50 && i<positivesQ.size(); i++) {
    	    System.out.printf("%-10s +%-10s\n", positivesQ.peek().getWord(), positivesQ.poll().getSentiment());
    	}
    	System.out.printf("\nThere are %s negative words above threshold %f\n", negativesQ.size(), threshold);
    	for (int i=0; i<50 && i<negativesQ.size(); i++) {
    	    System.out.printf("%-10s %-10s\n", negativesQ.peek().getWord(), negativesQ.poll().getSentiment());
    	}
	}
}
