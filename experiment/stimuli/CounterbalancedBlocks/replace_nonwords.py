#! /bin/env/ python3

import yaml
import re

with open("nonwords.yml", 'r') as infile:
    l = yaml.load(infile)

def nonword(l):
    for nw in l:
        yield nw

nonword = nonword(l)

for lnum in [1,2,3,4,5,6]:
    with open("{which}/{which}list.txt".format(which=lnum), 'r') as infile:
        words = infile.read().split('\n')

    print(list(filter(lambda x: "N" in x, words)))

    for word_index in range(len(words)):
        if "N" not in words[word_index]: continue
        words[word_index] = re.sub("(?<=\tN\t).*$", next(nonword), words[word_index])

    print(list(filter(lambda x: "N" in x, words)))

    with open("{which}/{which}list.txt".format(which=lnum), 'w') as outfile:
        for line in words:
            outfile.write(line+'\n')
