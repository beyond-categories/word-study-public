# author: Aalok S.

# for output to a javascript JSON-like object for jsPsych

import json
import os
# os.chdir('/home/aalok/code/word-study/experiment/stimuli/CounterbalancedBlocks')

try:
    # print(os.getcwd())
    # which_list = 1

    taskDict = {
        "N" : "lex",
        "R" : "rhy",
        "S" : "sem",
    }

    dictionary = dict()

    # print([k for k in dictionary.get('%dlist' % (which_list))])

    for which_list in range(1,6+1):
        dictionary['list%d' %((which_list+1)//2)] = dictionary.get(
            'list%d'%((which_list+1)//2), {
                "N" : {},
                "R" : {},
                "S" : {},
            }
        )
        for key in dictionary['list%d' % ((which_list+1)//2)]:
            with open('%d/%dlist.txt' % (which_list, which_list), 'r') as list_file:
                for line in list_file:
                    splitline = line.split() # ['11', 'W', 'journey']
                    dictionary['list%d'%((which_list+1)//2)][key][int(splitline[0]) + 243*((which_list+1) % 2)] = {
                        'word' : "<p class=\"stimulus\">" + splitline[2] + "</p>",
                        'data' : {
                            'text' : splitline[2],
                            'test_part' : 'stimulus',
                            'correct_response' : ' ' if (splitline[1] == key) else '#',
                            'task' : taskDict.get(key),
                            'attribute' : splitline[1],
                        },
                    }

    # print(dictionary)

    with open('word_stimuli.js', 'w') as out:
        out.write("var word_stimuli = ")
        json.dump(dictionary, out, indent=4, sort_keys=True)

except Exception as e:
    print(e)
            
print("done.\npress any key to continue", end='\r')
_ = input('\r')
print()
