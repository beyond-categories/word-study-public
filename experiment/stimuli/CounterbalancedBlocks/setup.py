#! /bin/env/ python3

from cx_Freeze import setup, Executable

base = None    

executables = [Executable("list_shuffle.py", base=base)]

packages = ["idna", "os", "random", "itertools"]
options = {
    'build_exe': {    
        'packages':packages,
    },
}

setup(
    name = "list_shuffler",
    options = options,
    version = "0.11",
    description = 'shuffles word lists',
    executables = executables
)

#from distutils.core import setup
#import py2exe
#
#setup(console=['list_shuffle.py'])
