from __future__ import print_function
import os
import random
import itertools

visual_sep = '='*4*16+'\n'

class Block:
    pairs = None
    words = None

    def __init__(self, rhyme=(), synonym=(), nonword=None, words=set(),
                 raw_data=None):
        if raw_data == None:
            self.pairs = {rhyme, synonym}
            self.words = words
            self.words.add((nonword,))

        else:
            self.pairs = set()
            self.words = set()
            for line_index in range(len(raw_data)):
                line = raw_data[line_index].split()
                if line[1] in {'R', 'S'}:
                    # DEBUG
                    # print(self.words, {(raw_data[line_index-1],)})
                    self.words.difference_update({(raw_data[line_index-1],)})
                    self.pairs.add((raw_data[line_index-1],
                                    raw_data[line_index]))
                else:
                    self.words.add((raw_data[line_index],))
        self.permute()
        self.iterindex = 0
        self.maxindex = len(self.permutation)

    def permute(self):
        self.permutation = list(self.words) + list(self.pairs)
        random.shuffle(self.permutation)
        self.permutation = [v for tup in self.permutation for v in tup]
        return self.permutation

    def __iter__(self):
        return self

    def __next__(self):
        self.iterindex += 1
        if self.iterindex <= self.maxindex:
            return self.permutation[self.iterindex-1]
        raise StopIteration


for i in range(1,6+1):
    print ("{0:.2f}% done".format(100*i/6))
    try:
        with open('{i}/{i}list.txt'.format(i=i), 'r') as wordlist:
            lines = [line for line in wordlist]
            blocks = [Block(raw_data=lines[i:9+i]) for i in range(0,len(lines),9)]
            random.shuffle(blocks)
            with open('{i}/{i}list_shuffled.txt'.format(i=i), 'w') as out:
                for block in blocks:
                    # DEBUG
                    # print(block.permutation)
                    for line in block:
                        print(line, end='', file=out)
    except FileNotFoundError:
        with open('../../{i}/{i}list.txt'.format(i=i), 'r') as wordlist:
            lines = [line for line in wordlist]
            blocks = [Block(raw_data=lines[i:9+i]) for i in range(0,len(lines),9)]
            random.shuffle(blocks)
            with open('../../{i}/{i}list_shuffled.txt'.format(i=i), 'w') as out:
                for block in blocks:
                    for line in block:
                        print(line, end='', file=out)

input('\npress any key to continue...')
