#! /bin/env python

# This file may be used to extract features from (a) word(s) list
# usage: python featureExtractor.py
# when prompted, provide the name of the file containing a list of words in the format:
# word1
# word2
# word3
# ...
# ...
# Note that there must also be a subdirectory in $PWD named "BgData/", which stores
# another wordfile by the name "count_1w.txt" of the format:
# word1		#occurrences
# word2		#occurrences 
# ...
# ...

########################################
##########	Import statements ##########
########################################

import re
import math
import hashlib
from LdMatrix import Levenshtein	# external file in the same directory
import cPickle						# to read data dumps from pre-processing
from progressPrinter import progressPrinter



class WordFeatures:
             
########################################
##########	Class variables ############
########################################
   
	stringsPath = None
	#outPath = None
	progress_bar = None
	hashes = None
	commons = None
	levenshtein = None
	numSubstrings = None
	numSyllables = None
	markovScores = None
	
	dataDict = None
	
########################################
##########	Class initializer ##########
########################################

	def __init__(self, path):#, out):
		self.stringsPath = path
		self.progress_bar = progressPrinter()
		#self.outPath = out
		self.hashes = []
		self.commons = []
		self.levenshtein = []
		self.numSubstrings = []
		self.numSyllables = []
		self.markovScores = []
		
		#	Finally to hold all the data
		self.dataDict = {}
		
		self.process()
		
########################################
##########	Class' main process ########
########################################

	def process(self):
		
		print "\nProcessing."
		self.progress_bar.print_progress(0,100)
	
		f = open(self.stringsPath, 'r')
		#o = open(outPath, 'w')
		words = f.read()
		f.close()
		#print lines,1
		words = re.split('\r\n|\n',words)
		#print words
		for j in range(len(words)) :
			if words[j] == '' :
				del(words[j])
			#print(words[i])
			else:
				words[j] = words[j].split('\t')[0]
		#print lines,2
		
##########	Making a dictionary of (word: rank,occurrences) pairs

		commonDict = {}
		f = open('BgData/count_1w.txt','r')
		lines = f.readlines()
		iterationIndex = 1		
		for line in lines:
			commonDict[line.split()[0]] = (iterationIndex, line.split()[1])
			iterationIndex += 1
			
##########	Making a bigrams transition matrix of the words in the "big" corpus.
		
		transition_matrix = {}
		with open("BgData/cpickle_transition_dict.data", 'rb') as dict_items_open:
			transition_matrix = cPickle.load(dict_items_open)
			
##########	Process the words one by one and call all feature extracting methods

		progress_counter = 0
		for word in words:
			# Extract hash codes / port codes
			self.hashes.append(self.hsh(word))
			# Extract the rank in terms of commonness of occurrence
			self.commons.append(self.common(word, commonDict))
			# Extract the number of valid substrings present in the word
			self.numSubstrings.append(self.getNumSubstrings(word, commonDict))
			# Get the number of syllables of a word
			self.numSyllables.append(self.getNumSyllables(word))
			# Get the Markov bigrams p-score of a word
			self.markovScores.append(self.getMarkovScore(word, transition_matrix))
			
			# Show progress using a progress bar
			progress_counter += 1
			self.progress_bar.print_progress(progress_counter,len(words))
			
			
##########	Bulk process to get a list of <word, average top levenshtein distance> pairs
		 #	because of costly operations
		 #	hindsight: could've used cPickle for this, similar to the Bigrams function.
		print "Getting Levenshtein distances"
		self.levenshtein = self.getLevenshteinD()
		
		
##########	Dump everything into iostream for now, while an output function is still underway

		#	A list of column headers in the order they should be in
		printable = [['word', 'hashCode', 'commonness', 'avgLevenshteinDist', 'numSubstrings',
						'numSyllables', 'markovScore', ]]
						
		self.dataDict = dict((key, []) for key in printable[0])
		
		iterator = 0
		for lis in (words, self.hashes, self.commons, self.levenshtein, self.numSubstrings, self.numSyllables, self.markovScores):
			#printable.append(lis)
			self.dataDict[printable[0][iterator]] = lis
			iterator += 1
			
		#print self.dataDict
        	
        	
##########	Function call to dump data using cPickle

		self.outputDataToFile(self.dataDict, printable[0])
        	
        	
########################################
##########	Actual class functions #####
########################################            
##########	Extraction of features #####
########################################

# 1 ##### Hash to get the port code
	def hsh(self, word):
		#o = open(outPath + "_hash_" + ".out.txt", 'w')
		hashStr = str(10 + int(hashlib.sha256(str(word)).hexdigest(),16))
		outLine = word + "\t\t" + hashStr[len(hashStr)-6:len(hashStr)] + '\n'
		#print outLine
		#o.write(outLine)
		#o.close()
		#print "Hashing."
		return hashStr

# 2 ##### Returns how common in top 10,000 the word is. If not found, returns default 12,999
	def common(self, word, dictionary):
		#print "Determining commonness."
		return dictionary.get(word, (12999,))[0]
                
# 3 ##### Computes avg Levenshtein distance with 10 closest words 
	# Implemented separately. See LdMatrix.py
	# Such computationally heavy methods will be implemented separately.
	def getLevenshteinD(self):
		path = self.stringsPath#raw_input("\nPlease input the name of the word list (must be in the same directory)\n")
		outPath = str(path).split('.')[0] + ".out.txt"
		instance = Levenshtein(path,outPath)
		return instance.getClosenessList()
		#return range(len(self.hashes))
			
# 4 ##### Computes the number of valid smaller substrings present in the word
	def getNumSubstrings(self, word, dictionary, threshold=3):
		count = 0
		for key in dictionary:
			if len(key) < len(word) and len(key) >= threshold and key in word:
				count += 1
		#print "Calculating the number of substrings."
		return count
		#return 0
		
# 5 ##### Computes the number of syllables in a supplied (usually English) word
	def getNumSyllables(self, word):
		lambdafunc = lambda w:len(''.join(" x"[c in"aeiouy"]for c in(w[:-1]if'e'==w[-1]else w)).split())
		#print "Computing the number of syllables."
		return lambdafunc(word)

# 6 ##### Computes character bi-gram probability products for a given word and generates a "score"
	def getMarkovScore(self, word, transition_matrix):	# here 'dictionary' is of the corpus words.
		
		p = 0.0
		for i in range(len(word)-1):
			p += math.log(transition_matrix[word[i]][word[i+1]] + 0.)
		#p *= 1. / (transition_matrix[word[i]][1] + 0.)
		p -= math.log((transition_matrix[word[len(word)-1]][1] + 0.))
		#print "Computing Markov Bi-gram score."
		return p
		
# 7 #### Phoneme N-gram markov score (N - TBD)
	def getMarkovPhonemeScore(self, word, phonemeMatrix):
		return None
				

########################################
##########	Function to output data in #
##########	an organized fashion #######
########################################

	
	def outputDataToFile(self, dataDict, namesList):
		with open("features_data.cPickle", "wb") as dict_items_save:
			cPickle.dump(dataDict, dict_items_save)
			
		f = open("wordlist.csv", "w")
		for title in namesList:
			f.write(title + ",")
			print title + ","
			
		f.write("\n")
		print
		
		f.close()
		
		for index in range(len(dataDict[namesList[0]])):
			f = open("wordlist.csv", "a")
			
			for title in namesList:
				f.write(str(dataDict[title][index]) + ",")
				print title + ":\t\t" + str(dataDict[title][index]) + ","
			f.write("\n")
			print
			f.close()
		
		
########################################
#######	Main process	################	
########################################
#######	Class instantiation ############
#######	and call to the main function ##
########################################

path = raw_input("\nPlease input the name of the word list\n")
#outPath = str(path).split('.')[0] + ".out.txt"
instance = WordFeatures(path)#,outPath)
wait = str(input("Finished."))
print wait
exit()
