#! /bin/env python


import re
import math
import hashlib
import itertools
from progressPrinter import progressPrinter


class Levenshtein:
				
	stringsPath = None
	outPath = None
	progress_bar = None
	
	lds = []
	word_Lds = []

	def __init__(self, path, out):
		self.stringsPath = path
		self.outPath = out
		self.progress_bar = progressPrinter()
		self.process()

	def process(self):
		f = open(self.stringsPath, 'r')
		f2 = open("BgData/count_1w.txt", "r")
		#o = open(outPath, 'w')
		words = f.read()
		big = []
		biglines = f2.readlines()
		
		for line in biglines:
			big.append(line.split()[0])
			#print big
		
		f.close()
		f2.close()
		#print lines,1
		words = re.split('\r\n|\n',words)
		#big = re.split('\r\n|\n',big)
		
		#print lines,2
		print "Processing %d words.\n"%len(words)
		progress_counter = 0
		self.progress_bar.print_progress(progress_counter,1.*len(words)*len(big)+len(words)*25.+5)
		termCount = -12000 
		# Process the words one by one and call all feature extracting methods
		
		for firstWord in words:
			termCount += 1
			if termCount > 3:
				break
			array = []
			for secondWord in big:
				array.append(self.iterative_levenshtein(firstWord, secondWord))
				progress_counter += 1
				self.progress_bar.print_progress(progress_counter,1.*len(words)*len(big)+len(words)*25.+5)
			self.lds.append(array)

		print self.lds	
		
		w = open("LdMatrix.csv", "w")
				
		w.write("\t,\t")
		for bigword in big:
			w.write(bigword + ",\t")
		w.write("\n")
		for i in range(len(words)):
			w.write(words[i]+",\t")
			for j in range(len(big)):
				#print words[i], words[j], self.lds[i][j]
				w.write(str(self.lds[i][j]) + ",\t")
			w.write("\n")
		w.close()
		
		closeness = open("LdClosenessOfWords.csv", "w")
				
		for i in range(len(words)):
			closeness.write(words[i] + ",\t")
			self.lds[i].sort()
			averageD = 0.0
			howMany = float(min(len(big)-1,25.0))
			for firstFew in range(int(howMany)):
				progress_counter += 1
				averageD += self.lds[i][1+firstFew] / howMany
				#print words[i], self.lds[i][1+firstFew]
				self.progress_bar.print_progress(progress_counter,1.*len(words)*len(big)+len(words)*25.+5)
			closeness.write(str(averageD) + "\n")
			self.word_Lds.append(averageD)
		closeness.close()
		
								
######### Extracting feature: Levenshtein distance
	### https://www.python-course.eu/python3_memoization.php
	
	def iterative_levenshtein(self, s, t):
		rows = len(s)+1
		cols = len(t)+1
		dist = [[0 for x in range(cols)] for x in range(rows)]
		# source prefixes can be transformed into empty strings 
		# by deletions:
		for i in range(1, rows):
			dist[i][0] = i
		# target prefixes can be created from an empty source string
		# by inserting the characters
		for i in range(1, cols):
			dist[0][i] = i
		
		for col in range(1, cols):
			for row in range(1, rows):
				if s[row-1] == t[col-1]:
					cost = 0
				else:
					cost = 1
				dist[row][col] = min(dist[row-1][col] + 1,      # deletion
									 dist[row][col-1] + 1,      # insertion
									 dist[row-1][col-1] + cost) # substitution
		
		#for r in range(rows):
			#print(dist[r])
		return dist[row][col]
		#print(iterative_levenshtein("flaw", "lawn"))
	
	def getClosenessList(self):
		return self.word_Lds
	
'''	
path = raw_input("\nPlease input the name of the word list (must be in the same directory)\n")
outPath = str(path).split('.')[0] + ".out.txt"
instance = Levenshtein(path,outPath)
wait = input("Finished.")
'''