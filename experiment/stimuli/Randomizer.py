#! /bin/env python

import random
import os
import distutils.dir_util
import cPickle

class Randomizer :
	"""Randomizes a pre-specified list of words and correlates `port codes' with each of them, stores it as a CPickle dictionary for later retrieval."""
	B = 31*4	# total number of smaller blocks
	C = 4		# number of larger blocks (`C' for categories)
	W = 8		# number of words within a (smaller) block
	counterB = 5# number of counterbalance orders
	
	####	To store shuffled lists according to counterbalance order	############
	shuffledWordLists = []
	
	def __init__(self) :
	
		############################################################################
		####	Attempt to read words from default path or take in new path	########
		############################################################################
		defaultWordListPath = '.defaultWordListPath'
		fileName = None
		if not os.path.isfile(defaultWordListPath) :
			fileName = input("What is the name of the file storing the word list? (Must be in same directory.)\n")
			whetherDefault = input("Set this path as default? (y/n)\n")
			if whetherDefault == 'y' :
				f = open('.defaultWordListPath', 'w')
				f.write(str(fileName))
				f.close()
		else :
			if input("Load default?\n") :
				f = open('.defaultWordListPath', 'r')
				fileName = f.read()
				f.close()
			else :
				fileName = input("What is the name of the file storing the word list? (Must be in same directory.)\n")
				whetherDefault = input("Set this path as new default? (y/n)\n")
				if whetherDefault == 'y' :
					f = open('.defaultWordListPath', 'w')
					f.write(str(fileName))
					f.close()
		
		############################################################################
		####	Read words and form a words list	################################
		wordListFile = open(fileName, 'r')
		wordList = wordListFile.readlines()	# this is going to be 186*4 words long
		
		############################################################################
		####	Create hierarchical directory structure to store all data	########
		distutils.dir_util.mkpath(os.getcwd() + "counterBalancedLists/")
		for i in range(self.counterB) :
			distutils.dir_util.mkpath(os.getcwd() + "counterBalancedLists/order%d/"%(1+i))
		
		############################################################################
		####	Shuffle lists and dump them in appropriate structures and dicts	####
		############################################################################
		allDict = {}
		categories = list(range(self.C))
		random.shuffle(categories)
		############################################################################
		for k in range(self.counterB) :
			self.shuffledWordLists.append(wordList)
			random.shuffle(self.shuffledWordList[i])
			counterBDict = {}
			########################################################################
			orderIndex = 1
			for i in categories :
				d = {
					'metadata' : {self.blockNames[i],},
				}
				outFile = open(os.getcwd() + "counterBalancedLists/order%d/block%d.txt"%(1+k,1+i), 'w')
				outFile.write(self.blockNames[i] + "\n")
				outFile.close()
				####################################################################
				for j in range(int(self.B/4.)) :
					outFile = open(os.getcwd() + "counterBalancedLists/order%d/block%d.txt"%(1+k,1+i), 'a')
					words = self.shuffledWordLists[k][6*j:6*(j+1)]
					randomIndex = self.getRandomFrom(7)
					words = words[:randomIndex+1] + self.additionalWords[i][j] + words[randomIndex+1:]
					for addingToDict in range(8*j,8*(j+1)) :
						d[addingToDict+1] = words[addingToDict % 8]
						outFile.write(str(addingToDict+1) + "\t" + str(words[addingToDict % 8]) + "\n")
					outFile.close()
				####################################################################
				outFile.close()
				with open(os.getcwd() + "counterBalancedLists/order%d/block%d.cPickle"%(1+k,1+i), "wb") as dict_items_save:
					cPickle.dump(d, dict_items_save)
				counterBDict[i+1] = d
				orderIndex += 1
			with open(os.getcwd() + "counterBalancedLists/order%d/counterbalance.cPickle"%(1+k), "wb") as dict_items_save:
				cPickle.dump(counterBDict, dict_items_save)
			allDict[k+1] = counterBDict
			########################################################################
		############################################################################
		with open(os.getcwd() + "counterBalancedLists/all.cPickle", "wb") as dict_items_save:
			cPickle.dump(allDict, dict_items_save)	
		
		##
		
	############################################################################
	####	Actual storage of `special' words to be inserted in each small	####
	####	word-block.														####
	############################################################################
	additionalWords = {
	
		'rhymingWordPairs' : {
				# 31 tuples of n=2 here
				
			},
		0 : self.additionalWords['rhymingWordPairs'],
			
		'synonymWordPairs' : {
				# 31 tuples of n=2 here
					
			},
		1 : self.additionalWords['synonymWordPairs'],
		
		'emotionalWordPairs' : {	# emotionally (equi)valent words
				# 31 tuples of n=2 here
			},
		2 : self.additionalWords['emotionalWordPairs'],
				
		'invalidWordPairs' : { # pair of 1 valid and 1 invalid constructed using that valid word <-- could ask about orthographic similarity
				# 31 tuples of n=2 here
			},
		3 : self.additionalWords['invalidWordPairs'],
			
	}
	
	############################################################################
	####	Indexed storage of block names for easy dumping into metadata	####
	blockNames = [
		'rhyming', 'synonym', 'emotional', 'lexical',
	]
	
	############################################################################
	####	Helper randomization function	####################################
	def getRandomFrom(self, n):
		if not n: return None
		l=list(range(n))
		random.shuffle(l)
		return l[0]