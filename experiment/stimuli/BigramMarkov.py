#! /bin/env python
# -*- coding: utf-8 -*-
# This file may be used to extract features from (a) word(s)
import re
import math
import hashlib
import itertools
import cPickle,sys
from progressPrinter import progressPrinter as progress

class BigramDictMaker:
				
	stringsPath = None
	outPath = None
	countsMatrix = {}
	progress_bar = None
	
####################################################################
########	Initialize and make call to process data	############
####################################################################
	def __init__(self, path, out):
		self.stringsPath = path
		self.outPath = out
		self.progress_bar = progress()
		self.process()


####################################################################
########	Read word list and make call to `makeMatrix'	########
####################################################################
	def process(self):
		
		# The path to the english words corpus
		f2 = open(self.stringsPath, "r")

		big = []
		biglines = f2.readlines()
		
		for line in biglines:
			big.append(line.split()[0])

		f2.close()

		print "\nProcessing\n"
		
		self.progress_bar.print_progress(0, len(big), prefix = 'Progress:', suffix = 'Complete', bar_length = 50)

####################################################################
		with open(self.outPath, "wb") as dict_items_save:
			cPickle.dump(self.makeMatrix(big, self.countsMatrix), dict_items_save)
			
				
####################################################################
########	Generate countsMatrix based on character transitions ###
####################################################################
	def makeMatrix(self, big, countsMatrix = {}):
	
		for char in 'abcdefghijklmnopqrstuvwxyz':	# only consider lowercase characters
			countsMatrix[char] = {1:0}
			for secondChar in 'abcdefghijklmnopqrstuvwxyz':
				countsMatrix[char][secondChar] = 0
		
		
		for j in range(len(big)):
			word = big[j]
			for i in range(len(word)-1):
				countsMatrix[word[i].lower()][1] += 1
				countsMatrix[word[i].lower()][word[i+1].lower()] += 1
				self.progress_bar.print_progress(j, len(big), prefix = 'Progress:', suffix = 'Complete', bar_length = 50)
		self.progress_bar.print_progress(len(big), len(big), prefix = 'Progress:', suffix = 'Complete', bar_length = 50)
		
		return countsMatrix

	
#path = raw_input("\nPlease input the name of the word list (must be in the same directory)\n")
outPath = "BgData/cpickle_transition_dict.data"
instance = BigramDictMaker("BgData/count_1w.txt",outPath)
try :
	wait = input("Finished.")
	print wait
except :
	pass
exit()