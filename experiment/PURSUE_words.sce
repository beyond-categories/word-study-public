    ########################################################################
    #------------------------------ PREAMBLE ------------------------------#
    ########################################################################
###################################################################################
### This scenario file accompanies the PURSUE `Perception of Words' Study.      ###
###                                                                             ###
### Design.                                                                     ###
### A stimulus word list is pre-generated and stored as six separate blocks.    ###
### Six counterbalance orders are present covering 3! permutations of tasks.    ###
### Counterbalance order determines:                                            ###
###     (1) the order in which to carry out tasks                               ###
###     (2) the order in which to use word lists                                ###
### Each whole experiment must be presented in six (6) iterations, manually,    ###
### since 8-bit port codes limit the number of stimuli in each. The particular  ###
### iteration of the ongoing block determines the word list to be used for      ###
### stimuli, based on the order determined by counterbalance.                   ###
### Words are successively presented in three tasks. Each stimulus lasts        ###
### 700 ms followed by an average pause of 700 ms (with a random +-100 ms       ###
### offset for staggered pausing).                                              ###
###                                                                             ###
### Tasks.                                                                      ###
###     (1) Lexical decision.                                                   ###
###         This task requires participants to carefully monitor the words      ###
###         being presented and respond each time they spot an invalid word.    ###
###     (2) Rhyming one-back decision.                                          ###
###         This task asks the participants to look a word in the context       ###
###         of its immediate previous stimulus.                                 ###
###         If a [current] word rhymes with the immediate previous word, they   ###
###         must respond.                                                       ###
###     (3) Semantic one-back decision.                                         ###
###         This task is similar in design to the previous task. It asks the    ###
###         participant to respond if the current word is related in meaning    ###
###         in some way to the previous word. Semantic relations include:       ###
###         synonyms, antonyms, hypernyms, superclass/category identifiers, etc.###
###                                                                             ###
### Stats for fun.                                                              ###
###     1 experiment; 6 blocks; 243 words per block.                            ###
###         1458 total stimuli; 972 stimuli to be used and 486 discarded.       ###
###         162 non-words, 162 rhyme pairs, 162 semantically related pairs.     ###
###                                                                             ###
###         threshold sentiment:        0.1                                     ###
###         approximate net sentiment:  -12.255406406229195                     ###
###         average sentiment per word: -0.26642187839628684                    ###
###         positive sentiment:         24.58938315977766                       ###
###         negative sentiment:         -36.84478956600686                      ###
###         positive words:             106                                     ###
###         negative words:             121                                     ###
###                                                                             ###
### Usage and logging.                                                          ###
###     To begin experiment, first, please go to `parameters' and select        ###
###     the appropriate counterbalance scheme from drop-down list. Then select  ###
###     the iteration number (which run you are currently on in this session).  ###
###     Then, proceed to the `main' tab and click `run'.                        ###
###     You will see a prompt. Kindly use both,                                 ###
###     (a) the participant identifier, and (b) the counterbalance order,       ###
###     while naming the current session before you begin.                      ###
###                                                                             ###
### Source code Git repo. (usage: `git clone /link/to/remote/repo')             ###
###     https://gitlab.com/pursue-word-study/back-end.git                       ###
###                                                                             ###
### Questions/bugs/problems/suggestions: A <aalok.sathe@richmond.edu>           ###
###                                                                             ###
###################################################################################
    ########################################################################
    #--------------------------- LICENSE NOTICE ---------------------------#
    ########################################################################

 #    This program is free software: you can redistribute it and/or modify    #
 #    it under the terms of the GNU General Public License as published by    #
 #    the Free Software Foundation, either version 3 of the License, or       #
 #    (at your option) any later version. You must cite the original authors. #
 #                                                                            #
 #    This program is distributed in the hope that it will be useful,         #
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of          #
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           #
 #    GNU General Public License for more details.                            #
 #                                                                            #
 #    You should have received a copy of the GNU General Public License       #
 #    along with this program.  If not, see <https://www.gnu.org/licenses/>.  #

    ########################################################################

################################
# Header file ##################
################################

scenario = "Perception of Words";

# Logitech gamePad button nos.: 4 [LEFT], 5 [RIGHT]
active_buttons = 3;
response_matching = simple_matching;
target_button_codes = 1,2,7;#,6,7;
response_logging = log_active;
response_port_output = true;
default_clear_active_stimuli = false;   # to make stimuli objects persist after presenting, for response time computation
write_codes = true;                     # need this to make trials and responses send out port codes

stimulus_properties =
    event_name,		string,
    trial_number,		number,
    stim_duration, 	number,
    pause_duration, 	number,
    p_code, 			number;

event_code_delimiter = ";";

################################
# SDL part #####################
################################
begin;

######## Placeholder text object to
######## encapsulate stimulus later
text {
    caption = "test";
    font_size = 60.0;
} stimulus_text_object;

######## Main trial event
trial {
	trial_duration = 1000;	# This value will be later set to <stim dur> + <pause dur>
	trial_type = fixed;

	##########################################
	###	Stimulus event to display words
	stimulus_event {
		picture {
			/*text stimulus_text_object;
			x=0; y=0;*/
			plane { width=1.0; height=1.0; emissive=1.0,1.0,1.0; } stimulus_holder_plane;
			x=0; y=0; z=0;
		};
		time = 1;
      stimulus_time_in = 0;
	   stimulus_time_out = 1000;	# Some temporarily hardcoded value because SDL definitions can't access parameter values.
		response_active = true;
		#target_button = 1,2;
		code = "Word_Stimulus_Display";
	} main_stimulus_event;

	##########################################
	###	Stimulus event to pause after words
	stimulus_event {
		picture {
			ellipse_graphic {
				ellipse_height = 10;
				ellipse_width = 10;
				#color = hehe;
			} fix_circ;
			x = 0; y = 0;
		} stim_pic;
		time = 701;
		response_active = false;
		#stimulus_time_in = 0;
	   #stimulus_time_out = 450;
		code = "Default_Fixation_Circle";
	} pause_event;

} main_trial;		# End trial

######## Pause trial to be shown only once, at the beginning of the experiment.
trial {
	trial_duration = 400;	# only a placeholder default; the actual value is randomly adjusted at runtime.
	trial_type = fixed;
	clear_active_stimuli = false;
	stimulus_event {
		picture {
			ellipse_graphic {
				ellipse_height = 10;
				ellipse_width = 10;
				#color = ;
			} pause_fixation_circ;
			x = 0; y = 0;
		} pause_fix_pic;
		response_active = false;
		code = "DefaultCircle";
	} init_pause_event;
} pause_trial;

######## Informational message to be shown initially and at the end of a block
trial {
	trial_duration = forever;
	trial_type = first_response;
	stimulus_event {
		picture {
			text {
				caption = "Block is over!";
				font_size = 40.0;
			} instruction_text_object;
			x = 0; y = 0;
			bitmap {
				filename = "./instruction_graphics/lexical_erp.png";
				preload = false;
			} instruction_bitmap;
			x = 0; y = 0;
		} instruction_picture;
		response_active = false;
		code = "Static instruction event";
	} instruction_event;

} instruction_trial;

################################
# PCL part #####################
################################
begin_pcl;

int stim_duration   = parameter_manager.get_int("stimulus_duration");
int pause_duration  = parameter_manager.get_int("pause_duration");
int counterbalance  = parameter_manager.get_int("order");
int which_iteration = parameter_manager.get_int("iteration");
int which_list      = (counterbalance % 3);
int task_order      = int(ceil(double(counterbalance)/3.0)); #which task order
int block           = int(floor(double(which_iteration+1)/2.0));

include_once "PURSUE_words_subroutine_implementations.pcl"; # includes the following subroutines:
#sub
#	array<string,2> read_file( string filename )
#sub
#	show_text_instruction( string instruction_text )
#sub
#	show_image_instruction( string instruction_image_path )

# read param manager and set target button for this iteration
array<string> target_buttons[2] = {"TOP RIGHT", "TOP LEFT"};
if (parameter_manager.get_string("target") == "shuffle")
then
	target_buttons.shuffle(); # randomly select an input button
elseif (parameter_manager.get_string("target") == "left")
then
	target_buttons[1] = target_buttons[2]; # set target button _[1] to "left"
end;

# Task numbers and corresponding names
# 1: lexical decision
# 2: rhyme decision
# 3: semantic decision
array<int> task_order_permutations[6][3] = {
    {1, 2, 3}, # 1
    {1, 3, 2}, # 2
    {2, 1, 3}, # 3
    {2, 3, 1}, # 4
    {3, 1, 2}, # 5
    {3, 2, 1}  # 6
};
int which_task = task_order_permutations[counterbalance][block];

array<int> word_list_order_counterbalance[6][6] = {
    {1,2,3,4,5,6},  # cb=1
    {3,4,1,2,5,6},  # cb=2
    {1,2,5,6,3,4},  # cb=3
    {5,6,3,4,1,2},  # cb=4
    {5,6,3,4,1,2},  # cb=5
    {1,2,3,4,5,6}   # cb=6
};

int num = word_list_order_counterbalance[counterbalance][which_iteration];

string path = "./stimuli/CounterbalancedBlocks/" + string(num) + "/" + string(num) + "list_shuffled.txt";
array<string> words[][] = read_file(path);

######## Instructions to be shown at the beginning of a task block as per the counterbalance scheme
array <string> instruction[3];
instruction[1] = "In this block, you will be presented with\nmany letter strings in a series, one after another.\n\nSome of these are valid English words and some are not.\n\nYour task is to identify\nletter strings that are NOT REAL.\n\nIf you think a letter string is NOT REAL,\nplease press the "+target_buttons[1]+" trigger as soon as you can.";
instruction[2] = "In this block, you will be presented with\nmany letter strings in a series, one after another.\n\nYour task is to identify if the current word RHYMES\nwith the word you saw IMMEDIATELY BEFORE it.\n\nIf you think a word rhymes with the previous word,\nplease press the "+target_buttons[1]+" trigger as soon as you can.";
instruction[3] = "In this block, you will be presented with\nmany letter strings in a series, one after another.\n\nYour task is to identify if the current word\nIS RELATED IN MEANING with the word you saw IMMEDIATELY BEFORE it\n(e.g., synonyms, or words from the same category).\n\nIf you think a word is related in meaning with the previous word,\nplease press the "+target_buttons[1]+" trigger as soon as you can.";

array <string> example[3];
example[1] = "For instance, if you see the following three words in a row, one-by-one:\n\n\"peducation\", \"bell\", and \"smell\"\n\nplease RESPOND with "+target_buttons[1]+" to \"peducation\",\nbecause it is NOT a valid English word.\n\nDO NOT respond to either \"bell\" or \"smell\",\nbecause they are VALID English words.";
example[2] = "For instance, if you see the following three words in a row, one-by-one:\n\n\"access\", \"ball\", and \"stall\"\n\nplease do NOT respond to \"access\"\nbecause there was no word before it.\n\nPlease do NOT respond to \"ball\"\nbecause the words \"access\" and \"ball\" don't rhyme.\n\nPlease RESPOND with "+target_buttons[1]+" to \"stall\",\nbecause the words \"ball\" and \"stall\" do RHYME with each other.";
example[3] = "For instance, if you see the following three words in a row, one-by-one:\n\n\"blue\", \"clue\", and \"hint\"\n\nplease do NOT respond to \"blue\"\nbecause there was no word before it.\n\nPlease do NOT respond to \"clue\"\nbecause the words \"blue\" and \"clue\" are\nNOT related in meaning with each other.\n\nPlease RESPOND with "+target_buttons[1]+" to \"hint\"\nbecause the words \"clue\" and \"hint\" are\nRELATED IN MEANING to each other.";
array <string> example_image[3];
example_image[1] = "./instruction_graphics/lexical_erp.png";
example_image[2] = "./instruction_graphics/phonological_erp.png";
example_image[3] = "./instruction_graphics/semantic_erp.png";

string wait_screen = "Please wait for the experimenter\nbefore beginning the experiment.";
string break_screen = "You may take a short break here.\nPress the trigger whenever you're ready.";

######## Show the instruction, then show an example screen, show a pause, and then begin main loop
show_text_instruction(instruction[which_task]);
show_text_instruction(example[which_task]);
show_image_instruction(example_image[which_task]);
show_text_instruction(wait_screen);

init_pause_event.set_event_code("INFO: task="+string(which_task)+" CB="+string(counterbalance)+" iteration="+string(which_iteration)+" list="+string(num));

countdown(parameter_manager.get_int("countdown_duration"));

pause_trial.set_duration(random(600,1000));
pause_trial.present();

######### Loop over all the word stimuli and present each.
loop
	int i = 1
until
	i > words[1].count()
begin

	int pause = 1+random(int(pause_duration*(1-0.25)),int(pause_duration*(1+0.25)));
	stimulus_text_object.set_caption(words[1][i]);					# Updating the word being shown
	main_stimulus_event.set_event_code(stimulus_text_object.caption() + ", " + words[2][i] + ", " + words[3][i]);	# Event code (string) for better log output
	main_stimulus_event.set_port_code(int(words[2][i]));			# The port code to send
	stimulus_text_object.redraw();										# Updating the graphic of the word
	stimulus_text_object.save_file("stimulus_text_object.bmp");					# Saving it by further retrieval by `plane' object
	stimulus_holder_plane.load_texture("stimulus_text_object.bmp");			# Setting plane to that word
	stimulus_holder_plane.set_size( stimulus_text_object.width(), stimulus_text_object.height() );
	#stimulus_holder_plane.mirror_x();									# Left-right inverted

	main_trial.get_stimulus_event(1).set_stimulus_time_out(stim_duration);
	main_trial.get_stimulus_event(2).set_stimulus_time_out(pause);
	main_trial.set_duration(stim_duration + pause);

	if ("NRS".substring(which_task,1).find(words[3][i]) != 0) # does the task in the current block match the word's intended target task?
	then
		main_stimulus_event.set_target_button({1,2});
		#DEBUG main_trial.set_duration(stim_duration+pause_duration+2000);
	else
		main_stimulus_event.set_target_button(0);					 # remove any prior assignment of target buttons
		main_stimulus_event.set_response_active(true);			 # set response to active so that trial becomes non-target active
	end;

	main_trial.present();									# present the `main' trial

	if (i % 81 == 0 && i != 243)
	then
		show_text_instruction(break_screen);
		countdown(parameter_manager.get_int("countdown_duration"));
	end;

	i = i + 1;
end;

########## Display finish message.
if (which_iteration < 6)
then
	instruction_text_object.set_caption("Iteration " + string(which_iteration) + " of 6 is over.\n\nPlease take a break while the\nexperimenters prepare the next iteration.\n\nPress the response button\nto end this iteration.");
else
	instruction_text_object.set_caption("Iteration " + string(which_iteration) + " of 6 is over.\n\nThank you for participating\nin this experiment!");
end;

instruction_event.set_event_code("INFO: "+get_total_info());

instruction_text_object.redraw();
instruction_trial.set_type(first_response);
instruction_trial.set_duration(forever);
instruction_trial.present();
