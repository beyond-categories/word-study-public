#! /bin/env/ python
from __future__ import print_function, division, absolute_import # python3 func


################################################################
# PREAMBLE
# an implementation of the PURSUE Word Study in PsychoPy
# NOTE: python2 required; python3 not supported
# coded by <Aalok Sathe> @ <University of Richmond, VA>
################################
license = """
    ########################################################################
    ---------------------------- LICENSE NOTICE ----------------------------
    ########################################################################

 #    This program is free software: you can redistribute it and/or modify    #
 #    it under the terms of the GNU General Public License as published by    #
 #    the Free Software Foundation, either version 3 of the License, or       #
 #    (at your option) any later version.                                     #
 #                                                                            #
 #    This program is distributed in the hope that it will be useful,         #
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of          #
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           #
 #    GNU General Public License for more details.                            #
 #                                                                            #
 #    You should have received a copy of the GNU General Public License       #
 #    along with this program.  If not, see <https://www.gnu.org/licenses/>.  #

    ########################################################################
"""
print(license)
################################
import datetime as dt # to prepare a timestamp
import time as tm # to supply absolute time for the timestamp
import os # for fs i/o
import shutil # fs i/o and more
import json,pickle # to read dumped stimuli
import itertools # for iteration and permutation-related stuff
import math # for basic math operations
# psychopy-specific stuff
import psychopy as pp # psychopy import and housekeeping stuff
from psychopy import visual as _ # anon. import to build cache
from psychopy import gui as _ # anon. import to support gui submodule
# END PREAMBLE
################################################################

################################
# a dictionary of params to be set conveniently at the beginning
params = {
    "experiment" : "Perception of Words",
    "version" : str(0.1),
    "timestamp" : dt.datetime.fromtimestamp(tm.time()).strftime('%Y%m%d_%H%M'),
    "subject_id" : "Foo Bar",
    "counterbalance" : range(1,6+1),
    "iteration" : range(1,6+1),
    "fullscreen" : [0, 1],
    "stimulus_duration" : [800]+[1]+range(50,2001,50), # in ms
    "fixation_duration" : [500]+[1]+range(50,1501,50), # in ms
    "fixation_deviation" : [200]+[1]+range(10,351,10), # in ms
    "logging" : [1, 0],
}

################################
# setting some top-level variables to be used later
monitor = pp.monitors.Monitor(pp.monitors.getAllMonitors()[0]) # active monitor
pwd = os.getcwd() # var to keep track of the working directory
pp.logging.console.setLevel(pp.logging.INFO) # select logging verbosity level
################################
# call pp.gui's dialog method to set expt params
if not pp.gui.DlgFromDict(dictionary=params, title="Experiment parameters",
                          fixed=["version", "timestamp",
                                 "experiment"],
                          order=["subject_id", "counterbalance", "iteration",
                                 "fullscreen", "stimulus_duration",
                                 "fixation_duration", "fixation_deviation",
                                 "logging", "version"],
                          tip={
                            "subject_id" : "string to identify the subject by",
                            "counterbalance" : "integer for randomization",
                            "iteration" : "within the current session",
                            "fullscreen" : "present experiment fullscreen",
                            "stimulus_duration" : "length for which stimulus"
                                                + " should be presented",
                            "fixation_duration" : "length of the interstimulus"
                                                + " interval duration",
                            "fixation_deviation" : "random offset of interval",
                            "logging" : "whether to log responses",
                            "version" : "experiment version",
                          }).OK:
    print("user pressed CANCEL, so quitting")
    pp.core.quit()
    raise SystemExit

################################
# dictionary for managing supplemental files
files = {
    "output_dir" : "out", # this is where logs and other data is to be outputted
    "logfile" : params["subject_id"] + '_'
                + params["counterbalance"] + '_'
                + params["iteration"] + '_'
                + params["timestamp"] + ".wordstudy.log", # unique logfile name
    "stimuli" : "./word_stimuli.json", # a pre-compiled json object for stimuli
}

################################
# call the gui dialog method to configure files
if not pp.gui.DlgFromDict(dictionary=files, title="File management").OK:
    print("user pressed CANCEL, so quitting")
    pp.core.quit()
    raise SystemExit
else:
    files["output_dir"] = os.path.join(pwd, files["output_dir"])
    if int(params["logging"]):
        pp.logging.LogFile(os.path.join(files["output_dir"], files["logfile"]),
                       level=pp.logging.INFO, encoding="utf8")
    ################################
    # make sure valid values were selected and configure filesystem accordingly
    if not os.path.isdir(files["output_dir"]):
        try:
            os.mkdir(files["output_dir"])
        except IOError:
            print("error creating or accessing output directory location")
            print("is '%s' r/w for the current user?" % files["output_dir"])
            pp.core.quit()
            raise SystemExit
    try:
        with open(os.path.join(pwd, files["stimuli"]),'r') as stimfile:
            stimuli = json.load(stimfile)
    except IOError:
        print("error accessing '%s'"%os.path.join(pwd, files["stimuli"]))
        print("did you supply correct path to the stimuli file?")
        pp.core.quit()
        raise SystemExit

# main window to run the experiment in
window = pp.visual.Window(monitor.getSizePix(), monitor=monitor,
                          fullscr=int(params["fullscreen"]),)

# placeholder stimulus and instructions holders
instruction_stim = pp.visual.TextStim(window, font="CMU Sans Serif",
                                  text="hello world", alignHoriz="center",
                                  alignVert="center", name="instructions")
word_stim = pp.visual.TextStim(window, font="CMU Sans Serif",
                              text="hello world", alignHoriz="center",
                              alignVert="center", name="stimulus_holder")
# instructions.setAutoDraw(True)
# stimulus.setAutoDraw(True)

################################################################
# main block
################################################################

def mktxtstim(text="hello world", font="CMU Sans Serif",
                     halign="center", valign="center", log=True, opacity=1.0):
    return pp.visual.TextStim(window, font=font, text=text, alignHoriz=halign,
                              alignVert=valign, autoLog=log, opacity=opacity)

def show_stim(window=None, stimulus=None, delay=1.0,
              text=None):
    if text is not None:
        stimulus.text = text
    stimulus.draw()
    window.flip()
    if delay >=0:
        keys = get_keypress()
        pp.core.wait(delay)
    else:
        return pp.event.waitKeys()

def shutdown(delay=.1):
    pp.core.wait(delay)
    window.close()
    pp.core.quit()

def get_keypress():
    keys = pp.event.getKeys()
    if 'escape' in keys:
        shutdown(0)
        raise SystemExit
    else:
        return keys

# show_stim(window=window, stimulus=word_stim, text="DEBUG")

task_order_permutations = itertools.permutations({1,2,3})
which_task = list(task_order_permutations)\
                    [int(params["counterbalance"])-1]\
                    [int(math.ceil(int(params["iteration"])/2))-1]

word_list_order_counterbalance = [
              [1,2,3,4,5,6],  # cb=1
              [3,4,1,2,5,6],  # cb=2
              [1,2,5,6,3,4],  # cb=3
              [5,6,3,4,1,2],  # cb=4
              [5,6,3,4,1,2],  # cb=5
              [1,2,3,4,5,6],] # cb=6
which_list = word_list_order_counterbalance\
                [int(params["counterbalance"])]\
                [int(params["iteration"])-1]

pp.logging.exp("counterbalance, %d"%int(params["counterbalance"]))
pp.logging.exp("iteration, %d"%int(params["iteration"]))
pp.logging.exp("task, %d"%which_task)
pp.logging.exp("word_list, %d"%which_list)

def read_stimuli(which_list=which_list):
    with open("../stimuli/CounterbalancedBlocks/%d/%dlist.txt"\
              %(which_list,which_list), 'r') as file:
        for line in file:
            print (line.split())
            yield line.split()

with open("instructions.pickle", 'r') as instrfile:
    instruction_dict = pickle.loads(instrfile.read())
    print(instruction_dict)
    instruction = instruction_dict[str(which_task)]["instruction"]
    example = instruction_dict[str(which_task)]["example"]

start_clock = pp.core.Clock() # start experiment clock for timings

show_stim(window=window, stimulus=instruction_stim, text=instruction, delay=-1)
show_stim(window=window, stimulus=instruction_stim, text=example, delay=-1)

for line in read_stimuli():
    show_stim(window=window, stimulus=word_stim, text=line[2], delay=1.)

pp.core.wait(1.) # to sustain final display
