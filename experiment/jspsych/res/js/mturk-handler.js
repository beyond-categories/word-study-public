// <!--
//     ########################################################################
//      LICENSE NOTICE
//     ########################################################################
//
//  #    This program is free software: you can redistribute it and/or modify
//  #    it under the terms of the GNU General Public License as published by
//  #    the Free Software Foundation, either version 3 of the License, or
//  #    (at your option) any later version. You must cite the original authors.
//
//  #    This program is distributed in the hope that it will be useful,
//  #    but WITHOUT ANY WARRANTY; without even the implied warranty of
//  #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  #    GNU General Public License for more details.
//  #
//  #    You should have received a copy of the GNU General Public License
//  #    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//  #
//  #  (C) 2017-2018 Aalok S.
//  #
//     ########################################################################
// -->


//$('#submitButton').hide();

$.extend({
  getUrlVars: function(){
    // From http://code.google.com/p/js-uri/source/browse/trunk/lib/URI.js
    var parser = /^(?:([^:\/?\#]+):)?(?:\/\/([^\/?\#]*))?([^?\#]*)(?:\?([^\#]*))?(?:\#(.*))?/;
    var result = window.location.href.match(parser);
    var scheme    = result[1] || null;
    var authority = result[2] || null;
    var path      = result[3] || null;
    var query     = result[4] || null;
    var fragment  = result[5] || null;

    if (query === null || query === undefined) {
      return {};
    }
    var vars = [], hash;
    var hashes = query.split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

$('#consent-link').each(function(){
    this.href += '&key=' + date;
})

const copy_to_clipboard = str => {
    const elem = document.createElement('textarea');
    elem.value = str;
    document.body.appendChild(elem);
    elem.select();
    document.execCommand('copy');
    document.body.removeChild(elem);
};

// auto-adjust text area
function adjustHeight(some_textarea){
    some_textarea.style.height = (some_textarea.scrollHeight > some_textarea.clientHeight) ? (some_textarea.scrollHeight)+"px" : "60px";
}

String.prototype.hashCode = function() {
    var hash = 0, i = 0, len = this.length;
    while ( i < len ) {
        hash  = ((hash << 5) - hash + this.charCodeAt(i++)) << 0;
    }
    return hash;
};
String.prototype.hashcode = function() {
    return (this.hashCode() + 2147483647) + 1;
};

$('#consent-link').click(function() {
    if ($.getUrlVar('assignmentId') == "ASSIGNMENT_ID_NOT_AVAILABLE") {
        alert("You must accept the HIT before being able to view the experiment.");
    } else {
        console.log($.getUrlVar('assignmentId'));
        $('#experiment-link').html('<a id="experiment-url" href="https://richmond.ca1.qualtrics.com/jfe/form/SV_85GHpzChuVEvg0t?mTurkID=' + $.getUrlVar('workerId') + '" target="_blank">this link</a>'); 
        $('#experiment-url').show();
    }
});

var pieces;
$('#verify-button').click(function() {
    try {
        pieces = atob($('#code-textarea').val()).split("DELIM");
        console.log(pieces[0], pieces[1]);
        var data = pieces[0];//dec(pieces[0], date);
        var hash = pieces[1];
        console.log(data);
        console.log(data.hashcode(), hash);
        if (data.hashcode() == hash) {
            alert("Verification successful! Please click Submit below to submit experiment progress and finish.");
            $('#data-text').val(data);
            $('#verify-button').text("Verification succeeded. Please submit the HIT.");
            $('#verify-button').prop('disabled',true);
            $('#code-textarea').hide();
            $('#verif-instr').hide();
            // $('#code-valid').show();
        } else {
            alert("Could not validate. Please try copying it again.");
            $('#verify-button').text("Verify. [TRY AGAIN!]");
        }
    } catch (err) {
        alert("It seems the code is invalid. Please try copying it again.");
        $('#verify-button').text("Verify. [TRY AGAIN!]");
    }
});
