// <!--
//     ########################################################################
//      LICENSE NOTICE
//     ########################################################################
//
//  #    This program is free software: you can redistribute it and/or modify
//  #    it under the terms of the GNU General Public License as published by
//  #    the Free Software Foundation, either version 3 of the License, or
//  #    (at your option) any later version. You must cite the original authors.
//
//  #    This program is distributed in the hope that it will be useful,
//  #    but WITHOUT ANY WARRANTY; without even the implied warranty of
//  #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  #    GNU General Public License for more details.
//  #
//  #    You should have received a copy of the GNU General Public License
//  #    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//  #
//  #  (C) 2017-2018 Aalok S.
//  #
//     ########################################################################
// -->


// This code block defines certain parameters used throughout the experiment
var experiment_params = {
    'stimulus_display_time' : 2000,      // Max length for which a stimulus will be displayed (ms). Default: 2000
    'fixation_average_time' : 700,       // Average duration of fixation after randomized offset (ms). Default: 700
    'fixation_random_offset': 150,       // Random offset in range [0 -- random_offset] to be added (ms)
    'show_data_at_end'      : false,     // Should all data be dumped as-is at the very end?
    'show_debrief_screen'   : true,      // Should participants be told how many trials they got right?
    'response_ends_trial'   : true,      // Every time button is pressed, stimulus hides
    '486'                   : 486,         // How many trials to show per subtask. Max: 486. Default: 486.
    'input_keys'            : [          // Defines which two keys should be used as input. Key order randomized.
        "e",
        "i",
    ],
};

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }

    return "empty";
};
// var date = getUrlParameter("key");
var assignmentIdparam = getUrlParameter("assignmentId");

// auto-adjust text area
function adjustHeight(some_textarea){
    some_textarea.style.height = (some_textarea.scrollHeight > some_textarea.clientHeight) ? (some_textarea.scrollHeight)+"px" : "60px";
}

String.prototype.hashCode = function() {
    var hash = 0, i = 0, len = this.length;
    while ( i < len ) {
        hash  = ((hash << 5) - hash + this.charCodeAt(i++)) << 0;
    }
    return hash;
};
String.prototype.hashcode = function() {
    return (this.hashCode() + 2147483647) + 1;
};
const copy_to_clipboard = str => {
    const elem = document.createElement('textarea');
    elem.value = str;
    document.body.appendChild(elem);
    elem.select();
    document.execCommand('copy');
    document.body.removeChild(elem);
};
