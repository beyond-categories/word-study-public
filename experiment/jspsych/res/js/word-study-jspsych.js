// <!--
//     ########################################################################
//      LICENSE NOTICE
//     ########################################################################
//
//  #    This program is free software: you can redistribute it and/or modify
//  #    it under the terms of the GNU General Public License as published by
//  #    the Free Software Foundation, either version 3 of the License, or
//  #    (at your option) any later version. You must cite the original authors.
//
//  #    This program is distributed in the hope that it will be useful,
//  #    but WITHOUT ANY WARRANTY; without even the implied warranty of
//  #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  #    GNU General Public License for more details.
//  #
//  #    You should have received a copy of the GNU General Public License
//  #    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//  #
//  #  (C) 2017-2018 Aalok S.
//  #
//     ########################################################################
//     -->

// $('#submitButton').hide();      // Hide MTurk's official submit button so participants don't end experiment early
// $('#jspsych-content').attr('align', 'center');
$('body').focus();              // Bring focus to body for keypresses to work on page open

var timeline = [];              // Declare timeline array to populate events as we go for later use with jsPsych

// ================================================================================================
// First set of instructions a participant will see; split into 4 pages
// ================================================================================================
var initialInstruction = {
    type: "instructions",
    pages: [
        "<h1>Instructions (1/4)</h1>"+
        "<p class=\"instruction\">" +
            "In this language-related experiment, you will see several letter strings, one after another, in " +
            "<strong>several different sub-tasks.</strong></p>",

            "<h1>Instructions (2/4)</h1>"+
            "<p class=\"instruction\">" +
            "In each subtask, you will be asked to judge each of the words shown to you according to some " +
            "specified criterion and respond using the keyboard.</p>",

            "<h1>Instructions (3/4)</h1>"+
            "<p class=\"instruction\">The entire experiment takes about 45 minutes to finish, and breaks "+
            "are available at the end of every sub-task, about 13 minutes apart. The maximum HIT time is "+
            "constrained to 2 hours, after which the task will expire.</p>",

            "<h1>Instructions (4/4)</h1>"+
            "<p class=\"instruction\">To re-read any of the instructions, simply use the <i>previous</i> button on the screen. "+
            "Once you're done reading these instructions, please press the <i>next</i> button to begin the "+
            "<strong>experiment.</strong></p>" +
        "</p>",
    ],
    show_clickable_nav: true,
    post_trial_gap: 1000,
};
timeline.push(initialInstruction)

// ================================================================================================
// Countdown group that shows "3, 2, 1, Go!" one-by-one
// ================================================================================================
var countdown_stimuli = [
    { count: "<p class=\"stimulus\">3</p>", data: { test_part: 'countdown' } },
    { count: "<p class=\"stimulus\">2</p>", data: { test_part: 'countdown' } },
    { count: "<p class=\"stimulus\">1</p>", data: { test_part: 'countdown' } },
    { count: "<p class=\"stimulus\">Go!</p>", data: { test_part: 'countdown' } },
];
var countdown = {
    type: "html-keyboard-response",
    stimulus: jsPsych.timelineVariable('count'),
    choices: [' '],
    data: jsPsych.timelineVariable('data'),
    trial_duration: 1001,
    response_ends_trial: false
}
var countdown_procedure = {
    timeline: [countdown],
    timeline_variables: countdown_stimuli,
    repetitions: 1,
    randomize_order: false
}
// End countdown group

// ================================================================================================
// Fixation plus mark that shows in-between stimuli and at the beginning.
// ================================================================================================
var fixationDisplay = {
    type: 'html-keyboard-response',
    stimulus: '<div style="font-size:60px;">+</div>',
    choices: jsPsych.NO_KEYS,
    trial_duration: function() {
        return experiment_params['fixation_average_time'] + Math.pow(-1, Math.floor(Math.random()*2)) * Math.floor(Math.random() * 250);
    }
};
timeline.push(fixationDisplay);

// ================================================================================================
// A simple Fisher-Yates shuffle implementation: ES6 version (https://stackoverflow.com/a/6274381)
// ================================================================================================
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

// ================================================================================================
// Randomly pick a `correct' key and an `incorrect' key from at least two keys
var key_list = experiment_params['input_keys'].map(function(elem) {
    return jsPsych.pluginAPI.convertKeyCharacterToKeyCode(elem);
});
shuffle(key_list);
// var which_key = {
//     69 : "E",
//     73 : "I",
// }

// ================================================================================================
// Defines array with elements representing individual lists from which to source word stimuli
var wordListOrder = [1, 2, 3];//, 4, 5, 6];
shuffle(wordListOrder);

// ================================================================================================
// Defines array with elements representing tasks from which to choose an order to perform tasks in
var taskListOrder = [1, 2, 3];
shuffle(taskListOrder);
var taskList = {
  'lex': '1',
  'rhy': '2',
  'sem': '3',
  1 : "N",
  2 : "R",
  3 : "S"
};

// ================================================================================================
// Array storing three instruction blocks for three tasks. Appropriate response keys inserted.
// UPDATE: flip the key order for the lexical task, to make associative sense to the participant
var taskInstructionsList = [
    'In this task, you will be shown a series of letter strings, one after another. Please press the "' +
    jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0])
    + '" button on your keyboard whenever you see <strong>a letter string\
    that is a VALID WORD</strong>. Carefully pay attention to all the letter strings, and press the "' +
    jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" button on your keyboard when you see a word that you think is <strong>INVALID</strong>. '
    + 'Please respond as rapidly as you can.',

    'In this task, you will be shown a series of letter strings, one after another. You must decide for each word whether it\
    rhymes with the word you saw immediately before it, so please pay close attention. Please press the\
    "'+ jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0]) +'" key on your keyboard <strong>if the current\
    word RHYMES with the word that appeared immediately before it</strong>. Please press the "'+
    jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1]) +'" key on your keyboard\
    if the current word does NOT rhyme. Words come and go fast, so please respond as rapidly as you can.',

    'In this task, you will be shown a series of letter strings, one after another. You must decide for each word whether it\
    is closely related in meaning with the word you saw right before it, so please pay close attention. Please\
    press the "'+ jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0]) +'" key on your keyboard <strong>if\
    you think the current word IS RELATED IN MEANING (e.g., synonym, same category, etc.) to the word that appeared right\
    before it</strong>. Please press the "'+ jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1]) +'" key on\
    your keyboard if you think the current word is NOT related in meaning with the word before it. Words come and\
    go fast, so please respond as rapidly as you can.'
]
var instructionPrefix = '<h1>Task-specific instructions. Read carefully!</h1><br><p id=\"task_instruction\" style=\"line-height: normal; font-family: Computer Modern Sans; font-size:33px; text-align: justify;\">'
var instructionSuffix = '</p><p>Press <i>next</i> to see an example.</p>'

// List of examples to be shown after instructions, each corresponding to a task (according to index)
var taskExamplesList = [
    '<img src="https://glcdn.githack.com/beyond-categories/word-study-public/raw/v0.4.8/experiment/instruction_graphics/lexical_js.png" width="600"><br>'
    + 'For instance, if you see the following three words in a row, one-by-one: '
    + '"peducation", "bell", and "smell", '
    + 'please respond to "peducation" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" because it is NOT a valid English word. '
    + 'Please respond to both of "bell" and "smell" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0])
    + '" because they are both VALID English words. <br>',

    '<img src="https://glcdn.githack.com/beyond-categories/word-study-public/raw/v0.4.8/experiment/instruction_graphics/phonological_js.png" width="600"><br>'
    + 'For instance, if you see the following three words in a row, one-by-one: '
    + '"access", "ball", and "stall", '
    + 'please respond to "access" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" because no word came before "access". '
    + 'Please respond to "ball" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" because "access" and "ball" do NOT rhyme with each other. '
    + 'Please respond to "stall" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0])
    + '" because "ball" and "stall" RHYME with each other. <br>',

    '<img src="https://glcdn.githack.com/beyond-categories/word-study-public/raw/v0.4.8/experiment/instruction_graphics/semantic_js.png" width="600"><br>'
    + 'For instance, if you see the following three words in a row, one-by-one: '
    + '"blue", "clue", and "hint", '
    + 'please respond to "blue" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" because no word came before "blue". '
    + 'Please respond to "clue" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[1])
    + '" because "blue" and "clue" are NOT related in meaning with each other. '
    + 'Please respond to "hint" using the key "'
    + jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(key_list[0])
    + '" because "clue" and "hint" ARE RELATED IN MEANING with each other. <br>'
]
var examplePrefix = '<h1>Task-specific examples. Study carefully!</h1><br><p id=\"task_example\" style=\"line-height: normal; font-family: Computer Modern Sans; font-size:33px; text-align: justify;\">'
var exampleSuffix = '</p><p>Press <i>next</i> to begin sub-task.</p>'

// ================================================================================================
// This array is predefined in an externally loaded javascript file containing a JSON object.
// var word_stimuli;// = [
//     {
//         word: "<p class=\"stimulus\">" + "word" + "</p>",
//         data: {
//             text: 'word',
//             test_part: 'stimulus',
//             correct_response: ' ',
//             task: 'which_task',
//             attribute: 'which_attr'
//         },
//     },
// ];

var current_iteration = 0;
var current_stimulus = 1;

// ================================================================================================
// Intermediate instruction display that is updated end of each task-block
var instruction = {
    // type: "html-keyboard-response",
    type: "instructions",
    pages: [
        "<p id=\"task_instruction\" style=\"line-height: normal; font-family: Computer Modern Sans; font-size:33px; text-align: justify;\">text.</p>",
        "<p id=\"task_example\" style=\"line-height: normal; font-family: Computer Modern Sans; font-size:33px; text-align: justify;\">text.</p>",
    ],
    // choices: [' '],
    // response_ends_trial: true,
    trial_duration: 60*1000,
    post_trial_gap: 1000,
    show_clickable_nav: true,
    allow_backward: true,
    // button_label_previous: "Left arrow key",
    // button_label_next: "Right arrow key",
    on_start: function(instruction) {
        instruction.pages[0] =  "<h1>Sub-task " + (current_iteration+1) + " of 3.</h1>" + instructionPrefix
                                + taskInstructionsList[taskListOrder[current_iteration]-1]
                                + instructionSuffix;
        instruction.pages[1] =  "<h1>Sub-task " + (current_iteration+1) + " of 3.</h1>" + examplePrefix
                                + taskExamplesList[taskListOrder[current_iteration]-1]
                                + exampleSuffix;
    },
    on_finish: function(data) {
        current_iteration++;
        current_stimulus = 1;
    },
}

// ================================================================================================
// Wrapper for presenting a word stimulus, populating trial data, and logging
var generic_stimulus = {
    type: "html-keyboard-response",
    stimulus: "dummy",
    choices: experiment_params['input_keys'],
    // data: jsPsych.timelineVariable('data'),
    trial_duration: experiment_params['stimulus_display_time'],
    timing_response: experiment_params['stimulus_display_time'],
    response_ends_trial: experiment_params['response_ends_trial'],
    // Executed right before the stimulus is presented
    on_start: function() {
        this.stimulus = word_stimuli["list" + wordListOrder[current_iteration-1]][taskList[taskListOrder[current_iteration-1]]][10+current_stimulus+""]["word"];
        this.data = word_stimuli["list" + wordListOrder[current_iteration-1]][taskList[taskListOrder[current_iteration-1]]][10+current_stimulus+""]["data"];
        this.data.response_expected = this.data.correct_response == ' ';
        this.data.yes_key = key_list[0];
        this.data.no_key = key_list[1];
        // For the lexical task, we flip yes/no buttons to make associative logical sense
        if (taskListOrder[current_iteration-1] == 1) {
            this.data.yes_key = key_list[1]; this.data.no_key = key_list[0];
        }
        if (this.data.response_expected) {
            this.choices = [this.data.yes_key];
        } else {
            this.choices = [this.data.no_key];
        }
        this.data.initial_incorrect = false;
    },
    on_load: function() {
        data = this.data;
        $("body").keypress(function(e) {
            if (!((e.which-32 == data.yes_key && data.response_expected)
                || (e.which-32 == data.no_key && !data.response_expected))) {
                data.initial_incorrect |= true;  // bitwise OR
                // console.log("initial incorrect");
            } else {
                data.initial_incorrect |= false; // bitwise OR
                // console.log("there");
            }
            // console.log(e.which-32, data.yes_key, data.no_key);
        });
        this.data.initial_incorrect = data.initial_incorrect;
    },
    // Executed right after the stimulus is presented
    on_finish: function(data) {
        data.correct = (data.key_press == data.yes_key && data.response_expected)
            || (data.key_press == data.no_key && !data.response_expected);
        data.correct &= !data.initial_incorrect;
        data.false_alarm = (data.key_press == data.yes_key) && !data.response_expected;
        data.missed = data.response_expected && !data.correct;

        console.log(data.correct, data.key_press, data.yes_key);
        this.data = data;
        // this.data.yes_key = jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(this.data.yes_key);
        // this.data.no_key = jsPsych.pluginAPI.convertKeyCodeToKeyCharacter(this.data.no_key);
        console.log(word_stimuli["list" + wordListOrder[current_iteration-1]][taskList[taskListOrder[current_iteration-1]]][10+current_stimulus+""]);
        current_stimulus++;
    },
}
// Container to wrap a stimulus and a fixation display together
var generic_procedure = {
    timeline: [fixationDisplay, generic_stimulus],
    // timeline_variables: word_stimuli,
    repetitions: experiment_params['486'],
    randomize_order: false,
}
// Wrap together a block or a task
var generic_task = {
    timeline: [instruction, countdown_procedure, fixationDisplay, generic_procedure],
    // timeline_variables: jsPsych.timelineVariable('data'),
    repetitions: 3,
    randomize_order: false,
}
timeline.push(generic_task);

// Adds a debrief message with some stats at the end of the experiment, if enabled in experiment_params
var debrief_block = {
    type: "html-keyboard-response",
    stimulus: function() {

        var trials = jsPsych.data.get().filter({test_part: 'stimulus'});
        var correct_trials = trials.filter({correct: true});
        var accuracy = Math.round(correct_trials.count() / trials.count() * 100);
        var rt = Math.round(correct_trials.select('rt').mean());

        return "<p class='instruction'>You responded correctly on "+accuracy+"% of the trials.</p>"+
        "<p class='instruction'>Your average response time was "+rt+"ms.</p>"+
        "<p class='instruction'>Press any key to continue to the completion instructions. Thank you!</p>";

    }
};
if (experiment_params['show_debrief_screen']) {
    timeline.push(debrief_block);
}

// ================================================================================================
// Initialize main process of going through jsPsych's timeline and displaying blocks of trials
jsPsych.init({
    timeline: timeline,
    // Some housekeeping to be done at the end
    on_finish: function() {
        if (experiment_params['show_data_at_end']) {
            jsPsych.data.displayData();
        }
        // Re-show MTurk's submit button so that code can now be submitted
        $('.jspsych-content-wrapper').hide()
        message_p = document.createElement('p');
        message_p.setAttribute("id", "end_msg_p");
        $('#end_message_div').prepend(message_p);
        // document.body.prepend(message_p);
        $('#end_msg_p').text("Please click the 'GENERATE' button above to generate a unique code. The code will be automatically copied to clipboard. Please PASTE this code into the  Mechanical Turk HIT display page in order to complete the task. Thank you for participating!");
        // $('#end_msg_p').css({"line-height": "normal", "font-family": "Computer Modern Sans", "font-size":"30px", "text-align": "justify"});
        $('#end_msg_p').css({"margin":"15px", "font-size":"27px", "text-align": "left"});
        submtbtn = document.createElement('button');
        submtbtn.setAttribute("id", "submitButton");
        $('#end_message_div').prepend(submtbtn);
        $('#submitButton').text("Generate");
        $('#submitButton').attr("data-clipboard-target", "#txtbox")
        // $('#submitButton').click(jsPsych.turk.submitToTurk(jsPsych.data));
        // function saveData(data) {
        //     var xhr = new XMLHttpRequest();
        //     xhr.open("POST", "SERVER:4242/response_listener/save_data");
        //     xhr.setRequestHeader('Content-Type', 'application/json');
        //     xhr.send(JSON.stringify({filedata: data}));
        // }

        csvdata = jsPsych.data.get().csv();
        console.log(csvdata.toString());

        $('#submitButton').click(function() {
            // $.post(
            //     "https://www.workersandbox.mturk.com/mturk/externalSubmit",
            //     {assignmentId: assignmentIdparam, csv: csvdata},
            //     function(data) {
            //         alert("Posted data. Please go back." + data);
            //     }
            // );
            data = csvdata;
            crypt = btoa(data.toString() + "DELIM" + data.toString().hashcode());
            copy_to_clipboard(crypt);

            txtbox = document.createElement("textarea");
            var clipboard = new ClipboardJS('#txtbox');
            txtbox.setAttribute("id", "txtbox");
            $('#end_message_div').prepend(document.createElement("br"));
            $('#end_message_div').prepend(txtbox);
            $("#txtbox").css({"width":$("#end_msg_p").width()});
            $("#txtbox").text(crypt);
            adjustHeight(txtbox);
            alert("Copied to clipboard! Please paste the code at the MTurk page.");

        });


    }
});
