%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[scale=1\iffalse1.24\fi,size=custom,width=142\iffalse56in\fi,height=86\iffalse34in\fi,]{beamerposter} % Use the beamerposter package for laying out the poster
%\setlength{\paperwidth}{56in}
%\setlength{\paperheight}{34in}


\usetheme{confposter} % Use the confposter theme supplied with this template

\usepackage{tikz}
\usepackage{xltxtra}
\usepackage{calc}
\usepackage{color}


\newlength{\mylength}

\usepackage{multirow, makecell}
\usepackage{rotating}
\settowidth\rotheadsize{Minimal}
\newcommand{\rot}[1]{\rotatebox{90}{#1}}
\newcommand{\multirot}[1]{\multirow{2}{*}{\rot{#1}}}

\setbeamercolor{block title}{fg=dblue\iffalse ngreen\fi,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464
% Set threecolwid to be (3*onecolwid)+2*sepwid = 0.708

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{142cm} % A0 width: 46.8in
\setlength{\paperheight}{86cm} % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth} % Separation width (white space) between columns
\setlength{\onecolwid}{0.22\paperwidth} % Width of one column
\setlength{\twocolwid}{0.464\paperwidth} % Width of two columns
\setlength{\threecolwid}{0.708\paperwidth} % Width of three columns
\setlength{\topmargin}{-0.9in} % Reduce the top margin size
%-----------------------------------------------------------

\usepackage{graphicx}  % Required for including images

\usepackage{booktabs} % Top and bottom rules for tables

%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{Understanding visual perception of words using an event-related potentials (ERP) megastudy} % Poster title
\author{Aalok Sathe\inst{1}, Cindy M. Bukach\inst{1}, Matthew W. Lowder\inst{1}, Jane W. Couperus\inst{2}, Catherine L. Reed\inst{3}} % Author(s)

\institute{
	\inst{1}University of Richmond,
	\inst{2}Hampshire College,
	\inst{3}Claremont McKenna College
	} % Institution(s)

%----------------------------------------------------------------------------------------
%	BEGIN DOCUMENT
%

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks
%\addtobeamertemplate{frametitle}{}{\vspace{-7em}}

%	UNIVERSITY ICON IN HEADER
\addtobeamertemplate{headline}{} 
{\begin{tikzpicture}[remember picture, overlay]
	\node [anchor=north west, inner sep=1.5cm]  at (current page.north west)
	{\includegraphics[scale=.18]{UR-Icon}};
	\end{tikzpicture}} % Logo next to the title
\addtobeamertemplate{headline}{} 
{\begin{tikzpicture}[remember picture, overlay]
	\node [draw,circle,anchor=north east, outer sep=1.3cm,inner sep=.12cm, text width=2.3cm, scale=3]  at (current page.north east)
	{Beyond\newline Categories 
		};
	\end{tikzpicture}} % Logo next to the title

\setlength{\belowcaptionskip}{1ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{frame}[t] % The whole poster is enclosed in one beamer frame
\vskip-2.9em

\begin{columns}[t] % The whole poster consists of four major columns, split into four columns - the [t] option aligns each column's content to the top

\setlength{\mylength}{4\onecolwid+5\sepwid}%
\begin{column}{\mylength}
\begin{columns}[t] % split into four columns
		
		
\begin{column}{\sepwid}\end{column} % Empty spacer column

%	CONTAINER FOR FIRST TWO COLUMNS
\setlength{\mylength}{2\onecolwid+\sepwid}
\begin{column}{\mylength}
	\begin{columns}[t]
	
\begin{column}{\onecolwid} % The first column

%----------------------------------------------------------------------------------------
%	MOTIVATION
%----------------------------------------------------------------------------------------


\setbeamercolor{block alerted body}{fg=black,bg=blue!5}
\begin{alertblock}{Motivation}
\sffamily
Humans are able to look at a variety of squiggly lines on all sorts of surfaces and extract meaning from them. This process is complex, and has several subprocesses it can be broken down into.
\iffalse{\begin{itemize}
\item
	How do we identify meaningful symbols amidst noise and string them together to form valid, sensible sequences?
\item
	How do we process these strings as a whole and associate some semantic entity with them?
\item
	How does meaning persist in memory
	%and is made sense of in a larger context
	?
\item
	How does language work in the brain?
\end{itemize}}\fi
Here, we try to work towards answering a specific question about the intermediate process between receiving a visual stimulus and understanding the meaning of a word:
{\bfseries how do we visually perceive words?}

\end{alertblock}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\setbeamercolor{block alerted title}{fg=black,bg=blue!7}
\setbeamercolor{block alerted body}{fg=black,bg=blue!7}
\begin{alertblock}{Introduction}

	{\bfseries Electrophysiology.\quad} It is the measurement of electric current and voltage changes that propagate to the surfaces of tissues. In the brain,
	the changes correspond neurons conducting electrochemical signals.		
	%{\bfseries EEG.\quad} Short for electroencephalography,
	EEG, short for electroencephalography, is an electrophysiological measure of brain activity collected at the scalp, accurate to milliseconds.
	
	{\bfseries ERP.\quad} Event-related potentials are EEG waveforms time-locked to some event. When averaged across participants, they allow generating representative waveforms corresponding to specific stimuli.
	
	{\bfseries The case for \textit{megastudies} in psycholinguistics.\quad} Often, researchers choose a small set of stimuli just enough to satisfy the statistical needs of their \textit{factorial} (hypothesis-specific) experiments.
	However, in psycholinguistics, such small sets of stimuli cannot represent the lexicon of any natural language, which can be arbitrarily large. Further, the stimuli may introduce unforeseen confound \cite{balota2012megastudies}. In \textit{megastudies}, researchers choose as many as several thousand stimuli, which can be expected to be representative of a natural language lexicon to a much larger extent. \textit{Megastudies} help build large datasets.
	

\end{alertblock}

%------------------------------------------------

%	PAST RESEARCH

\setbeamercolor{block alerted title}{fg=black,bg=blue!7}
\setbeamercolor{block alerted body}{fg=black,bg=blue!7}
\begin{alertblock}{Past research}
	
	{\bfseries Past research.\quad}In several studies involving ERP, researchers have primarily used the lexical decision task --- participants must decide if a stimulus is a word or a non-word. In such a case, word processing may be directed at any number of word features that indicate word
	status. In one \textit{megastudy}, researchers presented about 1100 words, also in a lexical decision task \cite{dufau2015} (see figure 1).
\end{alertblock}


%----------------------------------------------------------------------------------------

\end{column} % End of the first column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % Begin a column which is two columns wide (column 2)
	
%	RESEARCH QUESTIONS

\setbeamercolor{block alerted title}{fg=black,bg=blue!7}
\setbeamercolor{block alerted body}{fg=black,bg=blue!7}
\begin{alertblock}{Research questions}
	
	\begin{itemize}
		\item {\bfseries Task manipulation.\quad}
		Introducing tasks other than just lexical decision may change neural responses depending on what features of the words are relevant to the tasks. This possibility exists because they might require participants to process different linguistic information about the words.
		\item {\bfseries Individual differences.\quad}How do personal characteristics such as anxiety, depression, language proficiency, literary exposure, traumatic experiences, etc. affect the processing of different words?
		%\item {\bfseries Word valence/sentiment effect.\quad}Does the positive or negative valence/sentiment of a word cause a difference in processing in participants with certain personalities or traumatic experiences?
	\end{itemize}
	
\end{alertblock}
	
%	METHOD

\setbeamercolor{block alerted title}{fg=black,bg=blue!7}
\setbeamercolor{block alerted body}{fg=black,bg=blue!7}
\begin{alertblock}{Method}
	\begin{itemize}
		\item {\bfseries Participants.\quad}About 150 participants between ages 18 and 80 will be recruited from three college campuses and surroundings.
		
		\item {\bfseries Individual difference measures.\quad}
		Participants will complete some preliminary tests asking about their past experiences, measuring cognitive baseline, reading speed, literary exposure, etc.
		
		\item {\bfseries Stimuli.\quad}
		Participants will be shown 1458 words as 100pt white-on-black text. Each stimulus will last for 650ms followed by a randomized pause with duration between 300ms and 500ms so that participants don't get used to seeing the upcoming stimuli after the same pause. The entire list of stimuli will consist of blocks of 9 words, each containing 1 rhyming pair, 1 semantically related pair, 1 non-word, and 4 other non-special words. The list will be split into three sub-lists.
				
		\item {\bfseries Experiment design.\quad}
		The experiment will involve three tasks.
		\begin{enumerate}
			\item
			{\bfseries Lexical decision (``go/no-go'' task).}
			Decide if a word is a valid English word or not.
			
			\item
			{\bfseries Phonological one-back decision.}
			Click a button every time a word rhymes with its immediate previous stimulus.
			
			\item
			{\bfseries Semantic one-back decision.}
			Click a button every time a word is semantically related with its immediate previous stimulus.
			
		\end{enumerate}
		
		\item {\bfseries Counterbalance.\quad}
		Three blocks representing tasks will be counterbalanced to remove confound of task order in a cross design.
	\begin{equation*}\small
		\begin{bmatrix} 
		\text{Phonological decision} \\ 
		\text{Semantic decision} \\ 
		\text{Lexical decision}  
		\end{bmatrix}%_{\!|T|=3}
		\times
		\begin{bmatrix} 
		\text{${1},{2},{3}$} \\ 
		\text{${2},{3},{1}$} \\ 
		\text{${3},{1},{2}$}  
		\end{bmatrix}%_{|L|=3}
		=
		9 \text{ orders}
	\end{equation*}
	
	\qquad\qquad\qquad Tasks\qquad\qquad\qquad\quad\ Sub-list sequences

	\iffalse{\begin{table}
		\begin{tabular}{cl|c|c|c|}
%\toprule
	\cline{3-5}		
	& & \textbf{Sub-list A} & \textbf{Sub-list B} & \textbf{Sub-list C} \\  \cline{1-5}
\multicolumn{1}{|l}{}\multirow{3}{*}[1.5ex]{\rotcell{Task}}	&\multicolumn{1}{|l|}{\textbf{Lexical}} &1&2&3\\
	\multicolumn{1}{|l}{}	&\multicolumn{1}{|l|}{\textbf{Phonological}} &4&5&6\\
	\multicolumn{1}{|l}{}	&\multicolumn{1}{|l|}{\textbf{Semantic}} &7&8&9\\\hline
			
		\end{tabular}
		\caption{~Counterbalance orders ensuring an all-permute-all design}
		\label{counterbalance-table}
	\end{table}}\fi
		
	\end{itemize}
	
		
\end{alertblock}
	
\end{column} % End of the second column

\end{columns}

%	REST OF FIRST-TWO-COLUMNS HERE: THE GRAPHIC


%\iftrue
{\setlength{\mylength}{\twocolwid}
\begin{column}{\mylength}

%	DUFAU GRAPHIC
	
	\begin{block}{Time-course analyses of word processing}
		
\begin{figure}
	\includegraphics[width=.91\linewidth]{dufau_ERP_process_timeline}
	\caption{~Partial correlations between ERP voltages (indicated by color) and two word features at specific electrode sites. Numerically lower numbered electrodes are located towards the front of the head.
	%Word frequency is a measure of how commonly a word occurs in some predefined corpus. Orthographic Levenshtein distance is its average orthographic similarity, defined as the number of edits needed to convert a word into another, with its 10 closest neighbors.
	We can see that ERP voltages vary as a function of word frequency, positively between 200-300ms and 400-500ms, and negatively between 300-400ms.
	Adapted from \cite{dufau2015}.}
\end{figure}

\end{block}

\end{column}
}%\fi


\end{column}
%	END OF CONTAINER FOR FIRST TWO GROUPS

\begin{column}{\sepwid}\end{column} % Empty spacer column

%	CONTAINER FOR LAST TWO COLUMNS
\setlength{\mylength}{2\onecolwid+\sepwid}

\begin{column}{\mylength}
	\begin{columns}[t]
\begin{column}{\onecolwid}
	


%	EXAMPLE STIMULI

\setbeamercolor{block alerted body}{fg=black,} % Change the alert block body colors
\begin{alertblock}{Example stimuli}

\begin{figure}
	\includegraphics[width=.81\linewidth]{stimuli_sample_inkscape}
	\caption{~A typical block containing 9 stimuli. The block contains a non-word, one rhyming pair, one semantically related pair, and four other non-special stimuli. Three of these words elicit a response in some task, and must be omitted from the data due to motor interference.}
\end{figure}

\end{alertblock}

\end{column} % End of the third column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The fourth column

%----------------------------------------------------------------------------------------
%	EQUIPMENT
%----------------------------------------------------------------------------------------

\begin{block}{Equipment}

\begin{figure}
	\includegraphics[width=.92\linewidth]{dummy_eeg_cap}
	\caption{~A sample EEG cap to be placed on a participant during an experiment to collect real-time electro-physiological data.
		}
\end{figure}

\end{block}

%	FUTURE RESEARCH

\setbeamercolor{block alerted title}{fg=black,bg=blue!7}
\setbeamercolor{block alerted body}{fg=black,bg=blue!7}
\begin{alertblock}{Future exploration}

\begin{itemize}
	\item Do positively and negatively valenced words have any differential effect in the time-courses of perception of common word features?
	\item By-item analyses of words to understand where on the time-line of word perception particular word features are processed.
	\item Partial semantic recreation of stimuli based on brain activity. In other words, a computational model of word processing in the brain.
\end{itemize}	

	
\end{alertblock}

%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\begin{block}{References}

%\nocite{*} % Insert publications even if they are not cited in the poster
\small{\bibliographystyle{plain}
\bibliography{sample}%\vspace{0.75in}
}

\end{block}

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\setbeamercolor{block title}{fg=red,bg=white} % Change the block title color

\begin{block}{Acknowledgements}

\small{\rmfamily{This study falls under a larger project called PURSUE: Preparing Undergraduates for Research in STEM-related fields Using Electro-physiology, and is supported by grants from the National Science Foundation and the James S. McDonnell Foundation.}
	
	This poster was typeset in \XeLaTeX\ using a template made available by the Computational Physics and Biophysics Group, Jacobs University.
	}

\end{block}\vskip.4em

%----------------------------------------------------------------------------------------
%	CONTACT INFORMATION
%----------------------------------------------------------------------------------------

\setbeamercolor{block alerted title}{fg=black,bg=norange} % Change the alert block title colors
\setbeamercolor{block alerted body}{fg=black,bg=white} % Change the alert block body colors

\begin{alertblock}{Contact information}

\begin{itemize}
\item PURSUE Project web: \href{http://pursue.richmond.edu/}{http://pursue.richmond.edu/}
\item Lab web: \href{http://blog.richmond.edu/beyondcategories}{http://blog.richmond.edu/beyondcategories}
\item Email: \href{mailto:aalok.sathe@richmond.edu}{aalok.sathe@richmond.edu}
\end{itemize}

\end{alertblock}


%----------------------------------------------------------------------------------------

\end{column} % End of the fourth column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\end{columns}
\end{column}
%	END CONTAINER FOR LAST TWO COLS

\end{columns} % End of group of four columns
\end{column} % End column of columns

\end{columns} % End of container of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}
