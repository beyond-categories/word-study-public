## Visual Perception of Words using Event-Related Potentials
[![jspsy](https://img.shields.io/badge/depends%20on-jsPsych-brightgreen.svg)](https://www.jspsych.org/)
[![Presentation](https://img.shields.io/badge/depends%20on-NBS%20Presentation-brightred.svg)](https://www.neurobs.com/)
[![pipeline status](https://gitlab.com/beyond-categories/word-study-public/badges/public/pipeline.svg)](https://gitlab.com/beyond-categories/word-study-public/commits/public)

This repository houses a group of closely related ERP studies that involve
showing multiple words to participants. The words are shown as being in some
task, and the participants must act according to that task. Continuous EEG data
is collected which is then time-locked to individual *events* to find any
relations between events and brain activity.

To download the most recent build of the NBS Presentation files of this study,
please visit [this link](https://gitlab.com/beyond-categories/word-study-public/-/jobs/artifacts/public/download?job=package_presentation).
It will provide you a zip archive. Please unzip the archive and store it
wherever you wish. Please navigate to the directory `artifacts/experiment`.
To run the experiment, navigate to the `experiment` directory
and open the `.exp` file using Presentation. At the end of the experiment, logs
will be stored in the `experiment/logs` subdirectory. Please make sure to backup
the logs from your session if it was meant to be recorded. After you finish,
please **remove** the `artifacts` directory so that any updates will be pulled
the next time someone runs the experiment.

### Experiments are coded in
- NBS Presentation Languages: for use on Neurobehavioral Systems' Presentation
  software. The parent software, unfortunately, is non-free. However, the code
  supplied here is hosted under GPLv3.
- JavaScript + jsPsych: jsPsych is a psychology library in JavaScript, and helps
  design and code experiments that are portable, run in most modern browsers,
  and most importantly, is non-proprietary.
  The JavaScript code may also be used with Amazon Mechanical Turk (MTurk).
- In addition, the repository houses several python scripts that aided in
  stimuli generation, collection, data organization, and preliminary steps
  towards the inevitable data analysis.
- The repository contains a poster written in `LaTeX` that was presented at
  the University of Richmond's 30th annual Arts and Sciences symposium, 2018.

### Repo structure

```bash
.
├── BACKUP.txt
├── config_cp
├── Experiment
│   ├── experiment design.txt
│   ├── logs [14 entries exceeds filelimit, not opening dir]
│   ├── PURSUE_words.exp
│   ├── PURSUE_words.sce
│   ├── stimuli [24 entries exceeds filelimit, not opening dir]
│   ├── text1.bmp
│   └── Turk
│       ├── jsPsych [12 entries exceeds filelimit, not opening dir]
│       └── jspsych_turk-word-study.html
├── poster [20 entries exceeds filelimit, not opening dir]
├── PUBLIC
│   └── PUBLIC
└── README.md
```

#### ERP Experiment Counterbalance Scheme

| Iteration -->  | 1  | 2  | 3  | 4  | 5  | 6  |
|----------------|----|----|----|----|----|----|
| Counterbalance
| 1              | Lx | Lx | Rh | Rh | Sm | Sm |
| 2              | Lx | Lx | Sm | Sm | Rh | Rh |
| 3              | Rh | Rh | Lx | Lx | Sm | Sm |
| 4              | Rh | Rh | Sm | Sm | Lx | Lx |
| 5              | Sm | Sm | Lx | Lx | Rh | Rh |
| 6              | Sm | Sm | Rh | Rh | Lx | Lx |

### Possible future exploration:
- bilingual study with English <--> {Chinese, Hindi, Spanish}
    - show all the same words, in different conditions
        - English in Roman script
        - Other language in its original script
        - English in other language's script (where applicable)
        - Other language, Romanized
    - stimsets:
        - |{same lang, diff lang} X {same script, diff script}| = 4
- multilingual study with {English, Chinese, Hindi, Spanish}
    - show many words in each language, to monolinguals and bilinguals alike
